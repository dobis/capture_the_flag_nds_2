#include "flag.h"

void initFlag(flag_t* flag, struct player* owner, enum flag_type ft) {
    //Sanity check
    REQUIRE_NON_NULL(flag);
    REQUIRE_NON_NULL(owner);
    REQUIRE(ft == FLAG_1 || ft == FLAG_2);

    //Update the flag's owner
    flag->owner = owner;

    //Initialize flag
    switch (ft) {
    case FLAG_1:
        flag->x = FLAG_1_INIT_X_POS;
        flag->y = FLAG_1_INIT_Y_POS;
        break;

    case FLAG_2:
        flag->x = FLAG_2_INIT_X_POS;
        flag->y = FLAG_2_INIT_Y_POS;
        break;
    
    default:
        break;
    }
}

uint8_t flagGetXPos(flag_t* flag) {
    return flag->x / BOARD_GRANULARITY;
}

uint8_t flagGetYPos(flag_t* flag) {
    return flag->y / BOARD_GRANULARITY;
}

void resetFlag(flag_t* flag, enum flag_type ft){

    switch (ft) {
    case FLAG_1:
        flag->x = FLAG_1_INIT_X_POS;
        flag->y = FLAG_1_INIT_Y_POS;
        break;

    case FLAG_2:
        flag->x = FLAG_2_INIT_X_POS;
        flag->y = FLAG_2_INIT_Y_POS;
        break;
    
    default:
        break;
    }

}