#include "timer.h"

void initTimer() {
    //Initialize timer
    TIMER0_CR = TIMER_DIV_1024 | TIMER_IRQ_REQ | TIMER_ENABLE;
    TIMER0_DATA = TIMER_FREQ_1024(1);

    //Set interrupt handlers
    irqSet(IRQ_TIMER0, &(hitTimerISR));
}

void hitTimerISR() {
    if(p1Cooldown > 0) {
        --p1Cooldown;
    }
    if(p2Cooldown > 0) {
        --(p2Cooldown);
    }

    if((p1Cooldown == p2Cooldown) && (p2Cooldown == 0)) {
        stopHitTimer();
    }
}

void startHitTimer() {
    //Enable timer0 interrupts
    irqEnable(IRQ_TIMER0);
}

void stopHitTimer() {
    //Disable timer0 interrupts
    irqDisable(IRQ_TIMER0);
}
