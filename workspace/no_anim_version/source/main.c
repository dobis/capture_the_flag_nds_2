/*
 * Capture the flag Nintendo DS
 */

#include <nds.h>
#include <stdio.h>
#include "utils.h"
#include "Game_logic/game.h"
#include "graphics.h"
#include "controls.h"
#include "sound.h"

int main(void) {
    //Setup Graphics
    configureGraphics_Main();
    configureGraphics_Sub();

    //Initialize the timer
    initTimer();

    //Initialize music
    Audio_Init();

    //Inititalize game
    game_t* game = initGame(P1_VS_P2);

    //Play music
    Audio_PlayMusic();

    //Setup  backgrounds
    configureBG(MAIN);
    configureBG(SUB);   
    
    //Setup Sprites
    configureSprites();

    //flag to know when to copy back the old BG and touchscreen postition
    char was_paused = 0;

    enum TOUCH_TYPE tt = NO_PAUSE;

    //Main gameplay loop
    while(1) {
        
        if(game->is_paused) {
            handleTouch(&tt);
            if(tt == STOP) {
                break;
            } else if(tt == PLAY) {
                game->is_paused = 0;
            }
        } else {
            //Read held keys
    	    handleInput(game);
    	    tt = NO_PAUSE;
        }
        
        //Update the game's logical state
        updateGameState(game);
        
        //Check to see when the game was paused
        if(game->is_paused && !was_paused) {
            showMenu();
            was_paused = 1;
        }else if(!(game->is_paused) && was_paused) {
            showBG(SUB);
            was_paused = 0;
        }

        /**
         * TODO: Add update to game score (menu.h)
         */


    	swiWaitForVBlank();

    	oamUpdate(&oamMain);
    	oamUpdate(&oamSub);

    	if(game->has_winner != 0) {
            
            showScore(game);ù
            //add timer
            resetGame(game);
    	}


    }

    //End the game and return 
    endGame(game);
    return ERR_NONE;
}
