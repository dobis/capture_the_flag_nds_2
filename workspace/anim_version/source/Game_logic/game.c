#include "game.h"

game_t* initGame(game_mode_t mode) {
    //Sanity Check
    REQUIRE(mode == P1_VS_P2 || mode == P1_VS_CPU || mode == CPU_VS_CPU);

    //Allocate the space needed for all of the game components
    game_t* game = (game_t*)malloc(sizeof(game_t));
    REQUIRE_NON_NULL_VAL(game, NULL);

    game->board = (board_t)malloc(sizeof(enum slot) * GRID_SIZE);
    REQUIRE_NON_NULL_VAL(game->board, NULL);

    game->player1 = (player_t*)malloc(sizeof(player_t));
    REQUIRE_NON_NULL_VAL(game->player1, NULL);

    game->player2 = (player_t*)malloc(sizeof(player_t));
    REQUIRE_NON_NULL_VAL(game->player2, NULL);

    flag_t* flag1 = (flag_t*)malloc(sizeof(flag_t));
    REQUIRE_NON_NULL_VAL(flag1, NULL);

    flag_t* flag2 = (flag_t*)malloc(sizeof(flag_t));
    REQUIRE_NON_NULL_VAL(flag2, NULL);

    //Initialize the different components
    initBoard(game->board);
    initPlayer(game->player1, flag1, PLAYER_1);
    initPlayer(game->player2, flag2, PLAYER_2);
    initFlag(flag1, game->player2, FLAG_1);
    initFlag(flag2, game->player1, FLAG_2);

    //Set game mode and winner
    game->mode = mode;
    game->has_winner = 0;
    game->is_paused = 0;
    game->winner = NO_WIN;

    return game;
}

void endGame(game_t* game) {
    //Sanity check
    REQUIRE_NON_NULL(game);
    
    //Free and clear all elements of the game
    FREE_AND_CLEAR(game->board);
    FREE_AND_CLEAR(game->player1->flag);
    FREE_AND_CLEAR(game->player2->flag);
    FREE_AND_CLEAR(game->player1);
    FREE_AND_CLEAR(game->player2);
    FREE_AND_CLEAR(game);
}

char checkForCollisions(player_t* player, player_t* opp, int8_t moveX, int8_t moveY) {
    //Sanity check 
    REQUIRE_NON_NULL_VAL(opp, ERR);
    REQUIRE_NON_NULL_VAL(player, ERR);

    //Player's new position
    uint16_t playerX = player->x + moveX;
    uint16_t playerY = player->y + moveY;

    return (((playerX + HALF_SPRITE) >= opp->x && (playerY + HALF_SPRITE) >= opp->y) &&
        (playerX <= (opp->x + HALF_SPRITE) && playerY <= (opp->y + HALF_SPRITE)));

    //return 0;
}

char checkForWinner(game_t* game) {
    //Sanity check
    REQUIRE_NON_NULL_VAL(game, ERR);
    char result = 0;

    //Check if player 1 has the opponent's flag
    if(game->player1->has_flag) {
        uint8_t player1_x = playerGetXPos(game->player1);
        uint8_t player1_y = playerGetYPos(game->player1);

        //Check if the player is in his base
        if(isInBase(game->board, player1_x, player1_y)) {
            if((result = (player1_y < HALF_GRID_HEIGHT))) {
                game->has_winner = 1;
                game->winner = PLAYER_1;
                return result;
            }
        }
    }
    //Check if player 2 has the opponent's flag
    if(game->player2->has_flag) {
        uint8_t player2_x = playerGetXPos(game->player2);
        uint8_t player2_y = playerGetYPos(game->player2);

        //Check if the player is in his base
        if(isInBase(game->board, player2_x, player2_y)) {
            if((result = (player2_y > HALF_GRID_HEIGHT))) {
                game->has_winner = 1;
                game->winner = PLAYER_2;
                return result;
            }
        }
    }

    return result;
}

void updateGameState(game_t* game) {
    //Sanity check
    REQUIRE_NON_NULL(game);

    //Check to see if one of the players has picked up a flag
    if(!game->player1->has_flag) {
        game->player1->has_flag = checkForFlag(game->player1, game->player2->flag);
    }
    if(!game->player2->has_flag) {
        game->player2->has_flag = checkForFlag(game->player2, game->player1->flag);
    }

    //Check for the winner
    checkForWinner(game);

    //Update the board
    updateBoardState(game->board, game->player1, game->player2);
}

void updateBoardState(board_t board, player_t* player1, player_t* player2) {
    //Sanity check
    REQUIRE_NON_NULL(board);
    REQUIRE_NON_NULL(player1);
    REQUIRE_NON_NULL(player2);

    //Clear the board
    clearBoard(board);

    //Set the new position of the flag and the players
    uint8_t player1_x = playerGetXPos(player1);
    uint8_t player1_y = playerGetYPos(player1);
    uint8_t player2_x = playerGetXPos(player2);
    uint8_t player2_y = playerGetYPos(player2);
    uint8_t flag1_x = flagGetXPos(player2->flag);
    uint8_t flag1_y = flagGetYPos(player2->flag);
    uint8_t flag2_x = flagGetXPos(player1->flag);
    uint8_t flag2_y = flagGetYPos(player1->flag);

    slot_t player1_slot = isInBase(board, player1_x, player1_y) ?
                            (player1->has_flag ? PLAYER_FLAG_BASE : PLAYER_BASE) :
                            (player1->has_flag ? PLAYER_FLAG : PLAYER);

    slot_t player2_slot = isInBase(board, player2_x, player2_y) ?
                            (player2->has_flag ? PLAYER_FLAG_BASE : PLAYER_BASE) :
                            (player2->has_flag ? PLAYER_FLAG : PLAYER);

    slot_t flag1_slot = isInBase(board, flag1_x, flag1_y) ? BASE_FLAG : FLAG;
    slot_t flag2_slot = isInBase(board, flag2_x, flag2_y) ? BASE_FLAG : FLAG;

    updateSlot(board, player1_x, player1_y, player1_slot);
    updateSlot(board, player2_x, player2_y, player2_slot);

    if(!player1->has_flag) {
        updateSlot(board, flag1_x, flag1_y, flag1_slot);
    }

    if(!player2->has_flag) {
        updateSlot(board, flag2_x, flag2_y, flag2_slot);
    }

    return;
}

void printState(game_t* game) {
    //Sanity check
    REQUIRE_NON_NULL(game);
    if(game->has_winner == 0) {
        size_t x, y;
        for (y = 0; y < GRID_HEIGHT; ++y) {
            for(x = 0; x < GRID_WIDTH; ++x) {
                    printf("%d", ((game->board)[(y * GRID_WIDTH) + x]));
            }
            printf("\n");
        }
        printf("\n");
    } else {
        printf("%s has won this round !\n", game->has_winner == 1 ? "Player1" : "Player2");
    }
	
}


void resetGame(game_t* game){

    resetPlayer(game->player1);
    resetPlayer(game->player2);
    resetFlag(game->player1->flag, FLAG_1);
    resetFlag(game->player2->flag, FLAG_2);
    
}
