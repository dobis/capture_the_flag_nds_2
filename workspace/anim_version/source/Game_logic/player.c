#include "player.h"

void initPlayer(player_t* player, struct flag* flag, enum player_type pt) {
    //Sanity check
    REQUIRE_NON_NULL(player);
    REQUIRE_NON_NULL(flag);
    REQUIRE(pt == PLAYER_1 || pt == PLAYER_2);

    //Initialize player fields
    player->flag = flag;
    player->has_flag = 0;
    player->pt = pt;

    switch (pt) {
    case PLAYER_1:
        player->x = PLAYER_1_INIT_X_POS;
        player->y = PLAYER_1_INIT_Y_POS;
        p1Cooldown = 0;
        player->dir = DOWN;
        player->sT = P_TOP;
        break;

    case PLAYER_2:
        player->x = PLAYER_2_INIT_X_POS;
        player->y = PLAYER_2_INIT_Y_POS;
        p2Cooldown = 0;
        player->dir = UP;
        player->sT = P_BOTTOM;
        break;
    
    default:
        break;
    }
}

uint8_t playerGetXPos(player_t* player) {
    return player->x / BOARD_GRANULARITY;
}

uint8_t playerGetYPos(player_t* player) {
    return player->y / BOARD_GRANULARITY;
}

uint8_t checkForFlag(player_t* player, struct flag* flag) {
    //Sanity check 
    REQUIRE_NON_NULL_VAL(player, ERR);
    REQUIRE_NON_NULL_VAL(flag, ERR);

    return (playerGetXPos(player) == flagGetXPos(flag)) &&
           (playerGetYPos(player) == flagGetYPos(flag));
}  

int isHittable(player_t* hitter, player_t* hittee, enum direction hitDir) {
    size_t dist = manhattanDist(hitter->x, hitter->y, hittee->x, hittee->y);

    //Check if the hit was a success
    switch (hitDir) {
    case UP:
        return dist < (SPRITE_SIZE * 2);
    case RIGHT:
        return dist < (SPRITE_SIZE * 2);
    case DOWN:
        return dist < (SPRITE_SIZE);
    case LEFT:
        return dist < (SPRITE_SIZE);
    default:
        break;
    }

    //If none of the directions worked, return an error 
    return -1;
}

int hitPlayer(player_t* hitter, player_t* hittee, enum hit_type hitType, enum direction hitDir) {

    //Check if the player is at hitting distance
    if(isHittable(hitter, hittee, hitDir) != 0 ) {

        switch (hittee->pt) {
            case PLAYER_1:
                //Set the hit player's cooldown 
                switch (hitType) {
                case SHORT_HIT:
                    p1Cooldown = SHORT_COOLDOWN;
                    break;
                case LONG_HIT:
                    p1Cooldown = LONG_COOLDOWN;
                default:
                    break;
                }
                break;
            case PLAYER_2:
                //Set the hit player's cooldown 
                switch (hitType) {
                case SHORT_HIT:
                    p2Cooldown = SHORT_COOLDOWN;
                    break;
                case LONG_HIT:
                    p2Cooldown = LONG_COOLDOWN;
                default:
                    break;
                }
                break;
            default:
                break;
        }
        //Push the player away in case of an attack
        switch (hitDir) {
            case UP:
                if(((int)hittee->y - HALF_SPRITE) > 0) {
                    hittee->y -= HALF_SPRITE;
                }
                break;
            case LEFT:
                if(((int)hittee->x - HALF_SPRITE) > 0) {
                    hittee->x -= HALF_SPRITE;
                }
                break;
            case RIGHT:
                if((hittee->x + HALF_SPRITE) < GRID_WIDTH) {
                    hittee->x += HALF_SPRITE;
                }
                break;
            case DOWN:
                if((hittee->y + HALF_SPRITE) < GRID_HEIGHT) {
                    hittee->y += HALF_SPRITE;
                }
                break;
            default:
                break;
        }
        //Have the player drop the flag
        hittee->has_flag = 0;
        switch (hittee->pt) {
            case PLAYER_1:
                resetFlag(hitter->flag, FLAG_2);
                break;

            case PLAYER_2:
                resetFlag(hitter->flag, FLAG_1);
                break;
            default:
                break;
        }

        //Start cooldown timer
        startHitTimer();
        return 1;
    } 
    return 0;
}

void resetPlayer(player_t* player){

    player->has_flag = 0;

    switch (player->pt) {
    case PLAYER_1:
        player->x = PLAYER_1_INIT_X_POS;
        player->y = PLAYER_1_INIT_Y_POS;
        p1Cooldown = 0;
        player->dir = DOWN;
        player->sT = P_TOP;
        break;

    case PLAYER_2:
        player->x = PLAYER_2_INIT_X_POS;
        player->y = PLAYER_2_INIT_Y_POS;
        p2Cooldown = 0;
        player->dir = UP;
        player->sT = P_BOTTOM;
        break;
    
    default:
        break;
    }

}
