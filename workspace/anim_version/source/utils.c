#include "utils.h"

void desmumePrint(const char* errmsg) {
    /*volatile asm(
            "mov r0, %0"
            "swi 0xFC"
            : "=r" (errmsg) :
        );*/
}

size_t manhattanDist(int x1, int y1, int x2, int y2) {
	return abs(x1 - x2) + abs(y1 - y2);
}
