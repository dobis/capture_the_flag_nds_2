/*
 * Capture the flag Nintendo DS
 */

#include <nds.h>
#include <stdio.h>
#include "utils.h"
#include "Game_logic/game.h"
#include "graphics.h"
#include "controls.h"
#include "sound.h"

int main(void) {
    //Setup Graphics
    configureGraphics_Main();
    configureGraphics_Sub();

    //Initialize the timer
    initTimer();

    //Initialize music
    soundInit();

    //Inititalize game
    game_t* game = initGame(P1_VS_P2);

    //Play music
    soundPlayMusic();

    //Setup  backgrounds
    configureBG(MAIN);
    configureBG(SUB);   
    

    PlayerSprite player1_sprite;
    PlayerSprite player2_sprite;

    //Setup Sprites
    configureSprites(&player1_sprite, &player2_sprite);

    //flag to know when to copy back the old BG and touchscreen postition
    char was_paused = 0;
    char has_started = 0;

    enum TOUCH_TYPE tt = NO_PAUSE;

    //Show the starting screen
    showStart();

    //Starting loop
    while(!has_started) {
        handleTouch(&tt);
        if(tt != NO_PAUSE) {
            has_started = 1;
        }
    }

    //Reset the graphics before the main loop
    resetGraphics();

    //Main gameplay loop
    while(1) {
        
        if(game->is_paused) {
            handleTouch(&tt);
            //If stop return to the starting screen
            if(tt == STOP) {
                game->is_paused = 0;
                has_started = 0;
                //Show the starting screen
                showStart();

                //Starting loop
                while(!has_started) {
                    handleTouch(&tt);
                    if(tt != NO_PAUSE) {
                        has_started = 1;
                    }
                }
                //Reset the graphics before the main loop
                resetGraphics();

            } else if(tt == PLAY) {
                game->is_paused = 0;
            }
        } else if(game->has_winner != 0) {
            //Decide which winning screen to show and wait for input
            showWin(game->winner);
            handleTouch(&tt);

            //If the screen was hit then continue playing
            if(tt != NO_PAUSE) {
                game->winner = NO_WIN;
                game->has_winner = 0;
                tt = NO_PAUSE;

                //reset the game to the initial state
                resetGame(game);
                resetGraphics();
            }
        } else {
            //Read held keys
    	    handleInput(game, &player1_sprite, &player2_sprite);
    	    tt = NO_PAUSE;

			//Update the game's logical state
			updateGameState(game);
        }
        
        //Check to see when the game was paused
        if(game->is_paused && !was_paused) {
            showMenu();
            was_paused = 1;

        }else if(!(game->is_paused) && was_paused) {
            showBG(SUB);
            was_paused = 0;
        }

        /**
         * TODO: Add update to game score (menu.h)
         */


    	swiWaitForVBlank();

        oamUpdate(&oamMain);
        oamUpdate(&oamSub);
    }

    //End the game and return 
    endGame(game);
    return ERR_NONE;
}
