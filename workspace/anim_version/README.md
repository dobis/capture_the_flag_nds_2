# Capture_the_flag_nds

Repository for our Capture the flag project for the Micro-programmed Embedded systems class 2019.

Plan 
========
### Idea
- Capture the flag that spans accross the 2 screens.   
- Controls using: 
    - Arrow-pad and _abxy_ buttons for movement.   
    - L & R buttons to hit the other player.   
       


### Components     

#### Players
Two 8-bit like players (eggnog+ style) that have the following capabilities:   
- 2 movement axis (left-right & up-down) _or_ Omnidirectional _sill haven't decided_.    
- Hitbox ==> one player can hit the other in order to make him drop his flag.   
- Player should be able to catch and drop a flag.   
- Players should each have their own _unique_ skin, name and animations.  

#### Board
The game board should contain the following:   
- At the beggining of the game, each player should start with their flags in their respective _bases_.   
- The bases should be able to detect the arrival of a player.   
- Have multiple boards, some simple and some dynamic (the board can knock out a player).   
- A player should win a point and reset the board and players.   

#### Computer controlled player  
Allow the player to play against a bot rather than an other player.   
Also allow two bots to play against eachother.   
Finally allow maybe a 4 player mode (2 players and 2 computer players) with 2 teams.  

#### Sound
Allow for music, folley, SFX, ext...    

#### WiFi  
Allow for an online multiplayer mode with 2 NDSs.   
This means that we would need to remap the buttons and stuff.   

#### Editors  
Allow for a level editor and weapon editor.   

#### Tournament mode
Maybe also allow for multiple players to play in a tounament together (up to 8 players).  

#### Modes 
__Tile mode__ for player and flag BG
__Rotoscale mode__ for static BG.
   
TODO
===========

### Graphics 
[x] file called __graphics.c__:   
[x] 1. Initialize MAIN engine with VRAM and activate two backgrounds: 1 for the sprites and 1 for BG.
[x] 2. Do the same for the sub engine.  
 
### Controls 
file called __controls.c__:  
1. Handle controls and activate related interrupts.   
2. On interrupt call the relative game logic function.   


### For next Week  

[ ] Add the other sprites (other player and flag).  
[ ] Add movement controls for the sprites.  
[ ] Refactor control code to be in controls.c.  
[ ] Implement board grid logic.  
[ ] Have a sprite be able to go from the MAIN to the SUB engine.  
[ ] Implement hitboxes    
