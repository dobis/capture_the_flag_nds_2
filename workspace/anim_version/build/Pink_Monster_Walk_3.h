
//{{BLOCK(Pink_Monster_Walk_3)

//======================================================================
//
//	Pink_Monster_Walk_3, 96x32@4, 
//	+ 48 tiles Metatiled by 4x4 not compressed
//	Total size: 1536 = 1536
//
//	Time-stamp: 2020-01-14, 22:41:28
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_PINK_MONSTER_WALK_3_H
#define GRIT_PINK_MONSTER_WALK_3_H

#define Pink_Monster_Walk_3TilesLen 1536
extern const unsigned int Pink_Monster_Walk_3Tiles[384];

#endif // GRIT_PINK_MONSTER_WALK_3_H

//}}BLOCK(Pink_Monster_Walk_3)
