
//{{BLOCK(WinTop)

//======================================================================
//
//	WinTop, 256x192@8, 
//	+ palette 256 entries, not compressed
//	+ 55 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 32x24 
//	Total size: 512 + 3520 + 1536 = 5568
//
//	Time-stamp: 2020-01-14, 22:34:17
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_WINTOP_H
#define GRIT_WINTOP_H

#define WinTopTilesLen 3520
extern const unsigned int WinTopTiles[880];

#define WinTopMapLen 1536
extern const unsigned short WinTopMap[768];

#define WinTopPalLen 512
extern const unsigned short WinTopPal[256];

#endif // GRIT_WINTOP_H

//}}BLOCK(WinTop)
