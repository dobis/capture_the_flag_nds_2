
//{{BLOCK(menu)

//======================================================================
//
//	menu, 256x192@8, 
//	+ palette 256 entries, not compressed
//	+ 140 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 32x24 
//	Total size: 512 + 8960 + 1536 = 11008
//
//	Time-stamp: 2020-01-14, 22:34:17
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_MENU_H
#define GRIT_MENU_H

#define menuTilesLen 8960
extern const unsigned int menuTiles[2240];

#define menuMapLen 1536
extern const unsigned short menuMap[768];

#define menuPalLen 512
extern const unsigned short menuPal[256];

#endif // GRIT_MENU_H

//}}BLOCK(menu)
