
//{{BLOCK(Pink_Monster_Walk_2)

//======================================================================
//
//	Pink_Monster_Walk_2, 64x32@4, 
//	Transparent color : 00,00,00
//	+ palette 16 entries, not compressed
//	+ 32 tiles not compressed
//	Total size: 32 + 1024 = 1056
//
//	Time-stamp: 2020-01-14, 22:34:17
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_PINK_MONSTER_WALK_2_H
#define GRIT_PINK_MONSTER_WALK_2_H

#define Pink_Monster_Walk_2TilesLen 1024
extern const unsigned int Pink_Monster_Walk_2Tiles[256];

#define Pink_Monster_Walk_2PalLen 32
extern const unsigned short Pink_Monster_Walk_2Pal[16];

#endif // GRIT_PINK_MONSTER_WALK_2_H

//}}BLOCK(Pink_Monster_Walk_2)
