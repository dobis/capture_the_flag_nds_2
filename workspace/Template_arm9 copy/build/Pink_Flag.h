
//{{BLOCK(Pink_Flag)

//======================================================================
//
//	Pink_Flag, 32x32@4, 
//	Transparent color : 00,00,00
//	+ palette 16 entries, not compressed
//	+ 16 tiles not compressed
//	Total size: 32 + 512 = 544
//
//	Time-stamp: 2020-01-14, 04:39:27
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_PINK_FLAG_H
#define GRIT_PINK_FLAG_H

#define Pink_FlagTilesLen 512
extern const unsigned int Pink_FlagTiles[128];

#define Pink_FlagPalLen 32
extern const unsigned short Pink_FlagPal[16];

#endif // GRIT_PINK_FLAG_H

//}}BLOCK(Pink_Flag)
