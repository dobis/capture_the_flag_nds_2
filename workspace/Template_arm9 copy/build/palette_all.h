
//{{BLOCK(palette_all)

//======================================================================
//
//	palette_all, 64x64@8, 
//	+ palette 256 entries, not compressed
//	+ 38 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 8x8 
//	Total size: 512 + 2432 + 128 = 3072
//
//	Time-stamp: 2020-01-14, 04:39:27
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_PALETTE_ALL_H
#define GRIT_PALETTE_ALL_H

#define palette_allTilesLen 2432
extern const unsigned int palette_allTiles[608];

#define palette_allMapLen 128
extern const unsigned short palette_allMap[64];

#define palette_allPalLen 512
extern const unsigned short palette_allPal[256];

#endif // GRIT_PALETTE_ALL_H

//}}BLOCK(palette_all)
