
//{{BLOCK(Dude_Monster_Walk_6)

//======================================================================
//
//	Dude_Monster_Walk_6, 192x32@4, 
//	Transparent color : 00,00,00
//	+ palette 16 entries, not compressed
//	+ 96 tiles Metatiled by 4x4 not compressed
//	Total size: 32 + 3072 = 3104
//
//	Time-stamp: 2020-01-14, 04:39:27
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_DUDE_MONSTER_WALK_6_H
#define GRIT_DUDE_MONSTER_WALK_6_H

#define Dude_Monster_Walk_6TilesLen 3072
extern const unsigned int Dude_Monster_Walk_6Tiles[768];

#define Dude_Monster_Walk_6PalLen 32
extern const unsigned short Dude_Monster_Walk_6Pal[16];

#endif // GRIT_DUDE_MONSTER_WALK_6_H

//}}BLOCK(Dude_Monster_Walk_6)
