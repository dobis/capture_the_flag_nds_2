
//{{BLOCK(bg)

//======================================================================
//
//	bg, 256x192@8, 
//	+ palette 256 entries, not compressed
//	+ 12 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 32x24 
//	Total size: 512 + 768 + 1536 = 2816
//
//	Time-stamp: 2020-01-14, 04:39:27
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_BG_H
#define GRIT_BG_H

#define bgTilesLen 768
extern const unsigned int bgTiles[192];

#define bgMapLen 1536
extern const unsigned short bgMap[768];

#define bgPalLen 512
extern const unsigned short bgPal[256];

#endif // GRIT_BG_H

//}}BLOCK(bg)
