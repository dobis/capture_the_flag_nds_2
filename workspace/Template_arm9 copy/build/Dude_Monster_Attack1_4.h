
//{{BLOCK(Dude_Monster_Attack1_4)

//======================================================================
//
//	Dude_Monster_Attack1_4, 128x32@4, 
//	Transparent color : 00,00,00
//	+ palette 16 entries, not compressed
//	+ 64 tiles Metatiled by 4x4 not compressed
//	Total size: 32 + 2048 = 2080
//
//	Time-stamp: 2020-01-14, 04:39:27
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_DUDE_MONSTER_ATTACK1_4_H
#define GRIT_DUDE_MONSTER_ATTACK1_4_H

#define Dude_Monster_Attack1_4TilesLen 2048
extern const unsigned int Dude_Monster_Attack1_4Tiles[512];

#define Dude_Monster_Attack1_4PalLen 32
extern const unsigned short Dude_Monster_Attack1_4Pal[16];

#endif // GRIT_DUDE_MONSTER_ATTACK1_4_H

//}}BLOCK(Dude_Monster_Attack1_4)
