
//{{BLOCK(Pink_Monster_Walk_Attack_6)

//======================================================================
//
//	Pink_Monster_Walk_Attack_6, 192x32@8, 
//	Transparent color : 00,00,00
//	+ palette 16 entries, not compressed
//	+ 96 tiles Metatiled by 4x4 not compressed
//	Total size: 32 + 6144 = 6176
//
//	Time-stamp: 2020-01-14, 04:39:28
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_PINK_MONSTER_WALK_ATTACK_6_H
#define GRIT_PINK_MONSTER_WALK_ATTACK_6_H

#define Pink_Monster_Walk_Attack_6TilesLen 6144
extern const unsigned int Pink_Monster_Walk_Attack_6Tiles[1536];

#define Pink_Monster_Walk_Attack_6PalLen 32
extern const unsigned short Pink_Monster_Walk_Attack_6Pal[16];

#endif // GRIT_PINK_MONSTER_WALK_ATTACK_6_H

//}}BLOCK(Pink_Monster_Walk_Attack_6)
