
//{{BLOCK(Pink_Monster_Walk_6)

//======================================================================
//
//	Pink_Monster_Walk_6, 192x32@4, 
//	Transparent color : 00,00,00
//	+ palette 256 entries, not compressed
//	+ 96 tiles Metatiled by 4x4 not compressed
//	Total size: 512 + 3072 = 3584
//
//	Time-stamp: 2020-01-14, 04:39:28
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_PINK_MONSTER_WALK_6_H
#define GRIT_PINK_MONSTER_WALK_6_H

#define Pink_Monster_Walk_6TilesLen 3072
extern const unsigned int Pink_Monster_Walk_6Tiles[768];

#define Pink_Monster_Walk_6PalLen 512
extern const unsigned short Pink_Monster_Walk_6Pal[256];

#endif // GRIT_PINK_MONSTER_WALK_6_H

//}}BLOCK(Pink_Monster_Walk_6)
