#include <nds.h>

#include "controls.h"
#include "graphics.h"
#include "Game_logic/game.h"


int keys = 0 ;
int touchedKeys = 0;

//Initialize screen position of players and flags
SCREEN_TYPE screen_p1 = 0;
SCREEN_TYPE screen_p2 = 1;

SCREEN_TYPE screen_f1 = 1;
SCREEN_TYPE screen_f2 = 0;

void handleInput(game_t* game, PlayerSprite *player1_sprite,PlayerSprite *player2_sprite) {

	scanKeys();
	keys = keysHeld();
	touchedKeys = keysDown();
	
	if(!(game->is_paused)) {

		//Check for pause
		if(touchedKeys && KEY_START) {
			game->is_paused = 1;
		}

		//Check that the player is allowed to move
		if(p1Cooldown == 0) {

			//Update Player 1 position
			if(keys & KEY_L) {
				player1_sprite->anim_type = ATTACK;  //need to change something so it does full animation when attacking
				hitPlayer(game->player1, game->player2, SHORT_HIT, game->player1->dir);
			}

			if(keys){ // ((keys & KEY_RIGHT) || (keys & KEY_UP) || (keys & KEY_LEFT) || (keys & KEY_DOWN) ){
				player1_sprite->anim_frame++;
				//player2_sprite->anim_frame++;

				if(player1_sprite->anim_frame >= SIX_FRAMES) {
					player1_sprite->anim_frame = 0;
				}
			}



			/*
					if(keys & KEY_L){
						player1_sprite->anim_type = WALK_ATTACK;

						//declenche timer for attack
					}
					else{
						player1_sprite->anim_type = WALK;

						}


						if(player2_sprite->anim_frame >= SIX_FRAMES) {
							player2_sprite->anim_frame = 0;
						}
					}
					*/

			if((keys & KEY_RIGHT) && (game->player1->x < (SCREEN_WIDTH - SPRITE_WIDTH))) {
				if(!checkForCollisions(game->player1, game->player2, 2, 0)) {
					game->player1->x += 2;
				}
				game->player1->dir = RIGHT;
			}
			if((keys & KEY_DOWN) && (game->player1->y <= (SCREEN_HEIGHT - SPRITE_HEIGHT))) {
				screen_p1 = 0;
				if(!checkForCollisions(game->player1, game->player2, 0, 2)) {
					game->player1->y += 2;
				}
				game->player1->dir = DOWN;
			}
			if((keys & KEY_DOWN) && ((game->player1->y >= (SCREEN_HEIGHT + SPRITE_HEIGHT) ) && (game->player1->y< (2*SCREEN_HEIGHT - SPRITE_HEIGHT)))) {
				screen_p1 = 1;
				if(!checkForCollisions(game->player1, game->player2, 0, 2)) {
					game->player1->y += 2;
				}
				game->player1->dir = DOWN;
			}
			if((keys & KEY_DOWN) && ((game->player1->y > (SCREEN_HEIGHT - SPRITE_HEIGHT) ) && (game->player1->y< (SCREEN_HEIGHT + SPRITE_HEIGHT)))) {
				screen_p1 = 2;
				if(!checkForCollisions(game->player1, game->player2, 0, 2)) {
					game->player1->y += 2;
				}
				game->player1->dir = DOWN;
			}
			if((keys & KEY_LEFT) && (game->player1->x  > 0)) {
				if(!checkForCollisions(game->player1, game->player2, -2, 0)) {
					game->player1->x -= 2;
				}
				game->player1->dir = LEFT;
			}
			
			if((keys & KEY_UP) && (game->player1->y > 0) && (game->player1->y<= (SCREEN_HEIGHT - SPRITE_HEIGHT) )) {
				screen_p1 = 0;
				if(!checkForCollisions(game->player1, game->player2, 0, -2)) {
					game->player1->y -= 2;
				}
				game->player1->dir = UP;
			}
			if((keys & KEY_UP) && (game->player1->y > 0) && (game->player1->y>= (SCREEN_HEIGHT + SPRITE_HEIGHT)) ) {
				screen_p1 = 1;
				if(!checkForCollisions(game->player1, game->player2, 0, -2)) {
					game->player1->y -= 2;
				}
				game->player1->dir = UP;

			}
			if((keys & KEY_UP) && (game->player1->y > (SCREEN_HEIGHT - SPRITE_HEIGHT) ) && (game->player1->y< (SCREEN_HEIGHT + SPRITE_HEIGHT))) {
				screen_p1= 2;
				if(!checkForCollisions(game->player1, game->player2, 0, -2)) {
					game->player1->y -= 2;
				}
				game->player1->dir = UP;

			}
		}

		//Check that the player is allowed to move
		if(p2Cooldown == 0) {

			//Update Player 2 position
			if(keys & KEY_R) {
				player2_sprite->anim_type = ATTACK;
				hitPlayer(game->player2, game->player1, SHORT_HIT, game->player2->dir); 
			}

			if(keys & (KEY_A || KEY_B || KEY_X || KEY_Y)) {
				if(keys & KEY_R) {
					player2_sprite->anim_type = WALK_ATTACK;
					hitPlayer(game->player2, game->player1, SHORT_HIT, game->player2->dir); 

				}
				else {
					player2_sprite->anim_type = WALK;
				}
			}


			if((keys & KEY_A) && (game->player2->x < (SCREEN_WIDTH - SPRITE_WIDTH))) {
				if(!checkForCollisions(game->player2, game->player1, 2, 0)) {
					game->player2->x += 2;
				}
				game->player2->dir = RIGHT;
			}
			if((keys & KEY_B) && (game->player2->y <= (SCREEN_HEIGHT - SPRITE_HEIGHT))) {
				screen_p2 = 0;
				if(!checkForCollisions(game->player2, game->player1, 0, 2)) {
					game->player2->y += 2;
				}
				game->player2->dir = DOWN;
			}
			if((keys & KEY_B) && (game->player2->y >= (SCREEN_HEIGHT + SPRITE_HEIGHT)) && (game->player2->y < (2*SCREEN_HEIGHT - SPRITE_HEIGHT))) {
				screen_p2 = 1;
				if(!checkForCollisions(game->player2, game->player1, 0, 2)) {
					game->player2->y += 2;
				}
				game->player2->dir = DOWN;
			}
			if((keys & KEY_B) && (game->player2->y > (SCREEN_HEIGHT- SPRITE_HEIGHT) && (game->player2->y < (SCREEN_HEIGHT + SPRITE_HEIGHT)))) {
				screen_p2 = 2;
				if(!checkForCollisions(game->player2, game->player1, 0, 2)) {
					game->player2->y += 2;
				}
				game->player2->dir = DOWN;
			}
			if((keys & KEY_Y) && (game->player2->x  > 0)) {
				if(!checkForCollisions(game->player2, game->player1, -2, 0)) {
					game->player2->x -= 2;
				}
				game->player2->dir = LEFT;
			}
			if((keys & KEY_X) && (game->player2->y  > 0) && (game->player2->y <= (SCREEN_HEIGHT - SPRITE_HEIGHT) )) {
				screen_p2 = 0;
				if(!checkForCollisions(game->player2, game->player1, 0, -2)) {
					game->player2->y -= 2;
				}
				game->player2->dir = UP;
			}
			if((keys & KEY_X) && (game->player2->y  > 0) && (game->player2->y >= (SCREEN_HEIGHT + SPRITE_HEIGHT))) {
				screen_p2 = 1;
				if(!checkForCollisions(game->player2, game->player1, 0, -2)) {
					game->player2->y -=2;
				}
				game->player2->dir = UP;
			}
			if((keys & KEY_X) && (game->player2->y  > (SCREEN_HEIGHT - SPRITE_HEIGHT)) && (game->player2->y < (SCREEN_HEIGHT + SPRITE_HEIGHT))) {
				screen_p2 = 2;
				if(!checkForCollisions(game->player2, game->player1, 0, -2)) {
					game->player2->y -= 2;
				}
				game->player2->dir = UP;

			}
		}

		//Update Flag 1 position (flag 1 belongs to player 1 and is initialized in in player 1 base ( NOT on other side of arena))
		if(game->player1->has_flag) {
			game->player1->flag->x = game->player1->x;
			game->player1->flag->y = game->player1->y;
		}

		//Update Flag 2
		if(game->player2->has_flag) {
			game->player2->flag->x = game->player2->x;
			game->player2->flag->y = game->player2->y;
		}
	}
	

	//Should maybe reset when switching animation??



	/*
	else if(!keys){
		player1_sprite->anim_type = IDLE;
		player2_sprite->anim_type = IDLE;

		if((player1_sprite->anim_type == IDLE) || (player1_sprite->anim_type ==ATTACK)){
			player1_sprite->anim_frame++;
			if(player1_sprite->anim_frame >= FOUR_FRAMES) player1_sprite->anim_frame = 0;
		}

		if((player2_sprite->anim_type == IDLE) || (player2_sprite->anim_type ==ATTACK)){
			player2_sprite->anim_frame++;
			if(player2_sprite->anim_frame >= FOUR_FRAMES) player2_sprite->anim_frame = 0;
		}

	}
	*/

	//Display sprites
	showSprites(game, screen_p1, screen_p2, screen_f1, screen_f2, player1_sprite, player2_sprite);

  	//oamUpdate(&oamMain);
  	//oamUpdate(&oamSub);
}

void handleTouch(touchPosition* touch, enum TOUCH_TYPE* result) {
	scanKeys();
	keys = keysHeld();

	//Read the input from the touchScreen if there is one
	if (keys & KEY_TOUCH) {
		touchRead(touch);
		if(touch->px <= HALF_SCREEN) {
			*result = PLAY;
		} else if(touch->px > HALF_SCREEN) {
			*result = STOP;
		}
	}
}
