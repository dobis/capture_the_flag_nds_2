#include "graphics.h"
#include "bg.h"
#include "bg_SUB.h"
#include "Pink_Monster.h"
#include "Dude_Monster.h"
#include "Pink_Flag.h"
#include "Blue_Flag.h"
#include "Pink_Monster_Attack1_4.h"
#include "Pink_Monster_Walk_Attack_6.h"
#include "Pink_Monster_Walk_6.h"
#include "Pink_Monster_Idle_4.h"
#include "Dude_Monster_Attack1_4.h"
#include "Dude_Monster_Walk_Attack_6.h"
#include "Dude_Monster_Walk_6.h"
#include "Dude_Monster_Idle_4.h"
#include "menu.h"
//#include "palette_all.h"
#include <string.h>

void configureGraphics_Main() {
    //Setup in mode 5 with BG1 and BG3 active
    REG_DISPCNT = MODE_5_2D |  DISPLAY_BG0_ACTIVE;

    //Setup VRAM
    VRAM_A_CR = VRAM_ENABLE | VRAM_A_MAIN_BG;
}

/**
 *  @brief Configures the SUB engine in the same way as and activates BG3 and BG1. 
 *  corresponding VRAM bank
 */
void configureGraphics_Sub() {
    //Setup in mode 5 with BG1 and BG3 active
    REG_DISPCNT_SUB = MODE_5_2D | DISPLAY_BG0_ACTIVE;

    //Setup VRAM
    VRAM_C_CR = VRAM_ENABLE | VRAM_C_SUB_BG;
}

void showBG(enum BUFFER_TYPE bT) {

	switch(bT) {
		case MAIN:
			//Affine Matrix Transformation
            REG_BG2PA = 256;
            REG_BG2PC = 0;
            REG_BG2PB = 0;
            REG_BG2PD = 256;

            //Copy background to memory
            swiCopy(bgPal, BG_PALETTE, bgPalLen/2);
            swiCopy(bgMap, BG_MAP_RAM(0), bgMapLen/2);
            swiCopy(bgTiles, BG_TILE_RAM(1), bgTilesLen/2);
			break;
		case SUB:
			//Affine Matrix Transformation
            REG_BG2PA_SUB = 256;
            REG_BG2PC_SUB = 0;
            REG_BG2PB_SUB = 0;
            REG_BG2PD_SUB = 256;

            //Copy background to memory
            swiCopy(bg_SUBPal, BG_PALETTE_SUB, bg_SUBPalLen/2);
            swiCopy(bg_SUBMap, BG_MAP_RAM_SUB(0), bg_SUBMapLen/2);
            swiCopy(bg_SUBTiles, BG_TILE_RAM_SUB(1), bg_SUBTilesLen/2);
            break;
	}
	
}

/**
 * 	@brief Configures the background BG3 in extended rotoscale mode using the 16-bit pixel mode (i.e. emulating framebuffer mode)
 * Configures the background BG1 in TILED mode. (BG0 will be kept for foreground elements later on)
 */
void configureBG(enum BUFFER_TYPE bT) {
    switch(bT) {
        case MAIN:
            //Initialize backgrounds
        	BGCTRL[0] = BG_COLOR_256 | BG_MAP_BASE(0) | BG_TILE_BASE(1) | BG_32x32;
			showBG(MAIN);
            break;
        case SUB:
            //Initialize backgrounds
            BGCTRL_SUB[0] = BG_COLOR_256 | BG_MAP_BASE(0) | BG_TILE_BASE(1) | BG_32x32;
			showBG(SUB);
			break;
    }
}

void showMenu() {
	//Affine Matrix Transformation
	REG_BG2PA_SUB = 256;
	REG_BG2PC_SUB = 0;
	REG_BG2PB_SUB = 0;
	REG_BG2PD_SUB = 256;

	//Copy background to memory
	swiCopy(menuPal, BG_PALETTE_SUB, menuPalLen/2);
	swiCopy(menuMap, BG_MAP_RAM_SUB(0), menuMapLen/2);
	swiCopy(menuTiles, BG_TILE_RAM_SUB(1), menuTilesLen/2);
}

void initPlayerSprite(PlayerSprite *sprite, u8* walk)//, u8* attack, u8* idle, u8* walk_attack)
{
	sprite->sprite_gfx_mem = oamAllocateGfx(&oamMain, SpriteSize_32x32, SpriteColorFormat_16Color);  //pointer to sprite display location
	sprite->sprite_gfx_mem_sub = oamAllocateGfx(&oamSub, SpriteSize_32x32, SpriteColorFormat_16Color);

	sprite->frame_gfx_walk = (u8*)walk;  //pointer to frame, to know which frame to display
	//sprite->frame_gfx_attack = (u8*)attack;
	//sprite->frame_gfx_idle = (u8*)idle;
	//sprite->frame_gfx_walk_attack = (u8*)walk_attack;
}

void animatePlayer(PlayerSprite *sprite, SCREEN_TYPE sTp )
{
	// TODO : Add switch to know what animation to use (idle, walk, attack, walk+attack)
	//switch will change frame and offset (which pointer is used)

	int frame = sprite->anim_frame;
	u8* offset = 0;

	offset = sprite->frame_gfx_walk + frame * 32*32;

	/*
	switch(sprite->anim_type){
		case IDLE :
			offset = sprite->frame_gfx_idle + frame * 32*32;
			break;
		case ATTACK :
			offset = sprite->frame_gfx_attack + frame * 32*32;
			break;
		case WALK :
			offset = sprite->frame_gfx_walk + frame * 32*32;
			break;
		case WALK_ATTACK :
			offset = sprite->frame_gfx_walk_attack + frame * 32*32;
			break;
	}*/

	switch(sTp){
		case TOP :
			dmaCopy(offset, sprite->sprite_gfx_mem, 32*32);
			break;
		case BOTTOM :
			dmaCopy(offset, sprite->sprite_gfx_mem_sub, 32*32);
			break;
		case BOTH :
			dmaCopy(offset, sprite->sprite_gfx_mem, 32*32);
			dmaCopy(offset, sprite->sprite_gfx_mem_sub, 32*32);
			break;
	}
}


void configureSprites(PlayerSprite *player1_sprite, PlayerSprite *player2_sprite) {
	//Set up memory bank to work in sprite mode (offset since we are using VRAM A for backgrounds)
	VRAM_B_CR = VRAM_ENABLE | VRAM_B_MAIN_SPRITE_0x06400000;

	//Set up VRAM for Sub engine
	VRAM_D_CR = VRAM_ENABLE | VRAM_D_SUB_SPRITE;



	//Initialize sprite manager and the engine
	oamInit(&oamMain, SpriteMapping_1D_32, false);  //changed 1D_32 to 1D_128
	oamInit(&oamSub, SpriteMapping_1D_32, false);


	//Initialize Players
	initPlayerSprite(player1_sprite, (u8*)Pink_Monster_Walk_6Tiles); //,(u8*)Pink_Monster_Attack1_4Tiles, (u8*)Pink_Monster_Idle_4Tiles, (u8*)Pink_Monster_Walk_Attack_6Tiles );
	//initPlayerSprite(player2_sprite, (u8*)Dude_Monster_Walk_6Tiles); //,(u8*)Dude_Monster_Attack1_4Tiles, (u8*)Dude_Monster_Idle_4Tiles, (u8*)Dude_Monster_Walk_Attack_6Tiles);


	//swiCopy(palette_allPal, &SPRITE_PALETTE[0 ] , palette_allPalLen);


	//PALETTES
	//Main engine
	dmaCopy(Pink_MonsterPal, &SPRITE_PALETTE[0] , Pink_MonsterPalLen/2 );
	dmaCopy(Dude_MonsterPal, &SPRITE_PALETTE[0]+ Pink_MonsterPalLen/2, Dude_MonsterPalLen/2 );

	//Sub Engine

	dmaCopy(Pink_MonsterPal, &SPRITE_PALETTE_SUB[0] , Pink_MonsterPalLen/2 );
	dmaCopy(Dude_MonsterPal, &SPRITE_PALETTE_SUB[0]+ Pink_MonsterPalLen/2, Dude_MonsterPalLen/2 );



	//TODO : change grit of attack and cwalk attack to be same as otehrs
	//figure out why it bugs lol

/*

	//Allocate space for the graphic to show in the sprite
	gfx_p1 = oamAllocateGfx(&oamMain, SpriteSize_32x32, SpriteColorFormat_16Color);
	gfx_p1_sub = oamAllocateGfx(&oamSub, SpriteSize_32x32, SpriteColorFormat_16Color);

	gfx_p2 = oamAllocateGfx(&oamMain, SpriteSize_32x32, SpriteColorFormat_16Color);
	gfx_p2_sub = oamAllocateGfx(&oamSub, SpriteSize_32x32, SpriteColorFormat_16Color);

*/

	//FLAGS
	gfx_f1 = oamAllocateGfx(&oamMain, SpriteSize_32x32, SpriteColorFormat_16Color);
	gfx_f1_sub = oamAllocateGfx(&oamSub, SpriteSize_32x32, SpriteColorFormat_16Color);

	gfx_f2 = oamAllocateGfx(&oamMain, SpriteSize_32x32, SpriteColorFormat_16Color);
	gfx_f2_sub = oamAllocateGfx(&oamSub, SpriteSize_32x32, SpriteColorFormat_16Color);




	//Sprite Main - Flag 1
	swiCopy(Pink_FlagPal, SPRITE_PALETTE + Pink_MonsterPalLen/2 + Dude_MonsterPalLen/2, Pink_FlagPalLen/2);
	swiCopy(Pink_FlagTiles, gfx_f1 , Pink_FlagTilesLen/2);

	//Sprite Sub - Flag 1
	swiCopy(Pink_FlagPal, SPRITE_PALETTE_SUB + Pink_MonsterPalLen/2 + Dude_MonsterPalLen/2, Pink_FlagPalLen/2);
	swiCopy(Pink_FlagTiles, gfx_f1_sub , Pink_FlagTilesLen/2);


	//Sprite Main - Flag 2
	swiCopy(Blue_FlagPal, SPRITE_PALETTE + Pink_MonsterPalLen/2 + Dude_MonsterPalLen/2 + Pink_FlagPalLen/2, Blue_FlagPalLen/2);
	swiCopy(Blue_FlagTiles, gfx_f2 , Blue_FlagTilesLen/2);

	//Sprite Sub - Flag 2
	swiCopy(Blue_FlagPal, SPRITE_PALETTE_SUB + Pink_MonsterPalLen/2 + Dude_MonsterPalLen/2 + Pink_FlagPalLen/2, Blue_FlagPalLen/2);
	swiCopy(Blue_FlagTiles, gfx_f2_sub , Blue_FlagTilesLen/2);


/*

	//Copy data for the graphic (palette and bitmap)
	//Sprite Main - Player 1
	//swiCopy(Pink_MonsterPal, &SPRITE_PALETTE[0], Pink_MonsterPalLen/2);
	swiCopy(Pink_MonsterTiles, gfx_p1, Pink_MonsterTilesLen/2);

	//Sprite Sub - Player 1
	//swiCopy(Pink_MonsterPal, &SPRITE_PALETTE_SUB[0], Pink_MonsterPalLen/2);
	swiCopy(Pink_MonsterTiles, gfx_p1_sub, Pink_MonsterTilesLen/2);



	//Sprite Main - Player 2
	//swiCopy(Dude_MonsterPal, SPRITE_PALETTE + Pink_MonsterPalLen/2, Dude_MonsterPalLen/2);
	swiCopy(Dude_MonsterTiles, gfx_p2 , Dude_MonsterTilesLen/2);

	//Sprite Sub - Player 2
	//swiCopy(Dude_MonsterPal, SPRITE_PALETTE_SUB + Pink_MonsterPalLen/2 , Dude_MonsterPalLen/2);
	swiCopy(Dude_MonsterTiles, gfx_p2_sub , Dude_MonsterTilesLen/2);
*/

	/*
	//Sprite Main - Player 3
	swiCopy(Owlet_MonsterPal, SPRITE_PALETTE + Pink_MonsterPalLen/2, Owlet_MonsterPalLen/2);
	swiCopy(Owlet_MonsterTiles, gfx_p2 , Owlet_MonsterTilesLen/2);

	//Sprite Sub - Player 3
	swiCopy(Owlet_MonsterPal, SPRITE_PALETTE_SUB + Pink_MonsterPalLen/2 , Owlet_MonsterPalLen/2);
	swiCopy(Owlet_MonsterTiles, gfx_p2_sub , Owlet_MonsterTilesLen/2);
	*/



}


void showSprites(game_t* game, SCREEN_TYPE sTp1, SCREEN_TYPE sTp2, SCREEN_TYPE sTf1, SCREEN_TYPE sTf2, PlayerSprite *player1_sprite, PlayerSprite *player2_sprite){


	animatePlayer(player1_sprite, sTp1);
	//animatePlayer(player2_sprite, sTp2);


	//PLAYER 1
	switch(sTp1){
		case TOP :
			oamSet(&oamMain, 	// oam handler
					0,				// Number of sprite
					game->player1->x, game->player1->y,			// Coordinates
					0,				// Priority
					0,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					player1_sprite->sprite_gfx_mem,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case BOTTOM :
			oamSet(&oamSub, 	// oam handler
					0,				// Number of sprite
					game->player1->x, game->player1->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					0,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					player1_sprite->sprite_gfx_mem_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case BOTH :
			oamSet(&oamMain, 	// oam handler
					0,				// Number of sprite
					game->player1->x, game->player1->y,			// Coordinates
					0,				// Priority
					0,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					player1_sprite->sprite_gfx_mem,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			oamSet(&oamSub, 	// oam handler
					0,				// Number of sprite
					game->player1->x, game->player1->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					0,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					player1_sprite->sprite_gfx_mem_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;
	}

	//PLAYER 2

	switch(sTp2){
		case TOP :
			oamSet(&oamMain, 	// oam handler
					1,				// Number of sprite
					game->player2->x, game->player2->y,			// Coordinates
					0,				// Priority
					1,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					player2_sprite->sprite_gfx_mem,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
					);
			break;

		case BOTTOM :
			oamSet(&oamSub, 	// oam handler
					1,				// Number of sprite
					game->player2->x, game->player2->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					1,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					player2_sprite->sprite_gfx_mem_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
					);
			break;

		case BOTH :
			oamSet(&oamMain, 	// oam handler
					1,				// Number of sprite
					game->player2->x, game->player2->y,			// Coordinates
					0,				// Priority
					1,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					player2_sprite->sprite_gfx_mem,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
					);
			oamSet(&oamSub, 	// oam handler
					1,				// Number of sprite
					game->player2->x, game->player2->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					1,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					player2_sprite->sprite_gfx_mem_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
					);
			break;
	}

	//FLAG 1
	switch(sTf1){
		case TOP :
			oamSet(&oamMain, 	// oam handler
					2,				// Number of sprite
					0, 16,  //game->player1->flag->x, game->player1->flag->y,			// Coordinates
					0,				// Priority
					2,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f1,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case BOTTOM :
			oamSet(&oamSub, 	// oam handler
					2,				// Number of sprite
					192/2, 150, //game->player1->flag->x, game->player1->flag->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					2,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f1_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case BOTH :
			oamSet(&oamMain, 	// oam handler
					2,				// Number of sprite
					game->player1->flag->x, game->player1->flag->y ,			// Coordinates
					0,				// Priority
					2,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f1,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			oamSet(&oamSub, 	// oam handler
					2,				// Number of sprite
					game->player1->flag->x, game->player1->flag->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					2,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f1_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;
	}

	//FLAG 2

	switch(sTf2){
		case TOP :
			oamSet(&oamMain, 	// oam handler
					3,				// Number of sprite
					192/2, 16, //game->player2->flag->x, game->player2->flag->y,			// Coordinates
					0,				// Priority
					3,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f2,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case BOTTOM :
			oamSet(&oamSub, 	// oam handler
					3,				// Number of sprite
					game->player2->flag->x, game->player2->flag->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					3,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f2_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case BOTH :
			oamSet(&oamMain, 	// oam handler
					3,				// Number of sprite
					game->player2->flag->x, game->player2->flag->y,			// Coordinates
					0,				// Priority
					3,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f2,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			oamSet(&oamSub, 	// oam handler
					3,				// Number of sprite
					game->player2->flag->x, game->player2->flag->y -  SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					3,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f2_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

	}

	//Update memory
	//oamUpdate(&oamMain);
	//oamUpdate(&oamSub);


}
