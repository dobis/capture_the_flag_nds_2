#pragma once
#include <nds.h>

#include "Game_logic/game.h"
#include "utils.h"

#define SPRITE_HEIGHT 32
#define SPRITE_WIDTH 32
#define SCREEN_HEIGHT 192
#define SCREEN_WIDTH 256

u16* gfx_p1;
u16* gfx_p1_sub;
u16* gfx_p2;
u16* gfx_p2_sub;
u16* gfx_f1;
u16* gfx_f1_sub;
u16* gfx_f2;
u16* gfx_f2_sub;

#define FOUR_FRAMES 6
#define SIX_FRAMES 4


enum BUFFER_TYPE
{
    MAIN,
    SUB
};

enum SPRITE_NUMBER
{
    PLAYER1,
    PLAYER2,
    FLAG1,
    FLAG2
};

typedef enum {
	TOP,
	BOTTOM,
	BOTH
} SCREEN_TYPE;

typedef enum {
	WALK,
	ATTACK,
	WALK_ATTACK,  //boost+attack
	IDLE,
	HURT,
} ANIMATION_TYPE;

typedef struct
{
	u16* sprite_gfx_mem;
	u16* sprite_gfx_mem_sub;

	u8*  frame_gfx_walk;
	u8*  frame_gfx_attack;
	u8*  frame_gfx_idle;
	u8*  frame_gfx_walk_attack;

	int state;
	ANIMATION_TYPE anim_frame;

	int anim_type;
}PlayerSprite;




/**
 * @brief This function sets up the main graphical engine to work in MODE 5 and activates 2 BGs (1 and 3).
 * It also sets up the corresponding VRAM bank
 */
void configureGraphics_Main();

/**
 *  @brief Configures the SUB engine in the same way as and activates BG3 and BG1. 
 *  corresponding VRAM bank
 */
void configureGraphics_Sub();

/**
 * 	@brief Configures the background BG3 in extended rotoscale mode using the 16-bit pixel mode (i.e. emulating framebuffer mode)
 * Configures the background BG1 in TILED mode. (BG0 will be kept for foreground elements later on)
 */
void configureBG(enum BUFFER_TYPE bT);

/**
 * @brief Copies data to replace BG with the menu BG
 */ 
void showMenu();

/**
 * @brief Shows the game's BG on a given engine
 */ 
void showBG(enum BUFFER_TYPE bT);

void initPlayerSprite(PlayerSprite *sprite, u8* walk);//, u8* attack, u8* idle, u8* walk_attack);

void animatePlayer(PlayerSprite *sprite, SCREEN_TYPE sTp);

/**
 * @brief configures the sprite engine
 */
void configureSprites(PlayerSprite *player1_sprite, PlayerSprite *player2_sprite);

/**
 * @brief updates the sprites locations
 */
void showSprites(game_t* game, SCREEN_TYPE sTp1, SCREEN_TYPE sTp2, SCREEN_TYPE sTf1, SCREEN_TYPE sTf2, PlayerSprite *player1_sprite, PlayerSprite *player2_sprite);

