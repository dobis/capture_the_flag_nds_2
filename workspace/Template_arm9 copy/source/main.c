/*
 * Capture the flag Nintendo DS
 */

#include <nds.h>
#include <stdio.h>
#include "utils.h"
#include "Game_logic/game.h"
#include "graphics.h"
#include "controls.h"
#include "sound.h"

int main(void) {
    //Setup Graphics
    configureGraphics_Main();
    configureGraphics_Sub();

    //Initialize the timer
    initTimer();

    //Initialize music
    Audio_Init();

    //Inititalize game
    game_t* game = initGame(P1_VS_P2);

    //Play music
    Audio_PlayMusic();

    //TODO : Move this declaration, probably in game, then give game to all functions

    PlayerSprite player1_sprite;
    PlayerSprite player2_sprite;

    player1_sprite.anim_type = WALK;
    player2_sprite.anim_type = WALK;

    //Setup Sprites
    configureSprites(&player1_sprite, &player2_sprite);

    //Setup  backgrounds
    configureBG(MAIN);
    configureBG(SUB);

    //flag to know when to copy back the old BG and touchscreen postition
    char was_paused = 0;
    touchPosition touch;
    enum TOUCH_TYPE tt;

    //Main gameplay loop
    while(1) {
        if(game->is_paused) {

        	showMenu();
        	was_paused = 1;

            handleTouch(&touch, &tt);
            if(tt == STOP) {
                clearBoard(game->board);
            } else if(tt == PLAY) {
                game->is_paused = 0;
            }
        } else {
        	showBG(SUB);
        	was_paused = 0;

            //Read held keys
    	    handleInput(game, &player1_sprite, &player2_sprite);
        }
        
        //Update the game's logical state
        updateGameState(game);
        
//        //Check to see when the game was paused
//        if(game->is_paused) //&& !was_paused) {
//
//        }else if((!(game->is_paused)) && was_paused) {
//
//        }

        /**
         * TODO: Add update to game score (menu.h)
         */


    	swiWaitForVBlank();

    	oamUpdate(&oamMain);
    	oamUpdate(&oamSub);

    	if(game->has_winner != 0) {
    		clearBoard(game->board);
    	}
    }

    //End the game and return 
    endGame(game);
    return ERR_NONE;
}
