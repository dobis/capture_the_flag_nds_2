#pragma once
#include <nds.h>
#include "Game_Logic/game.h"
#include "graphics.h"

#define HALF_SCREEN 128

enum TOUCH_TYPE {
    PLAY,
    STOP
};

/**
 * @brief handles user input, like touchscreen and different buttons
 */
void handleInput(game_t* game, PlayerSprite *player1_sprite,PlayerSprite *player2_sprite);

/**
 * @brief handles all user input from the touchscreen
 * @param touch, a pointer to where the position of the touchScreen input will be stored
 * @param result, a pointer to where the result will be stored (1 if continue, 2 if stop and 0 if nothing)
 */ 
void handleTouch(touchPosition* touch, enum TOUCH_TYPE* result);

