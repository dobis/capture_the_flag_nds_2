#ifndef __BOARD_H
#define __BOARD_H

#include "player.h"
#include "../utils.h"

#define GRID_HEIGHT 48
#define HALF_GRID_HEIGHT 24
#define GRID_WIDTH 32
#define GRID_SIZE GRID_HEIGHT * GRID_WIDTH
#define BOARD_GRANULARITY 8
#define SIDE_WIDTH 8
#define BASE_WIDTH 16
#define BASE_HEIGHT 4

typedef enum slot {
    NONE = -1,
    EMPTY = 0,
    BASE,
    BASE_FLAG,
    PLAYER,
    FLAG,
    PLAYER_FLAG,
    PLAYER_BASE,
    PLAYER_FLAG_BASE,
    ENVIRONMENT
} slot_t;

typedef slot_t* board_t;

/*
Board initial state:
    0 0 0 0 0 0 0 0  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 3 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0

    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 3 0 0 0 0 0 0 0 0  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1  0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1  0 0 0 0 0 0 0 0
*/

/**
 * @brief Initializes the given board by setting all of its slots to their initial values
 * @param board, pointer to the board we want to initialize 
 */
void initBoard(board_t board);

/**
 * @brief Updates the slot of the given board at the given coordinates
 * @param board, the board that needs to be updated
 * @param x, the screen x coordinate of the slot that will be checked
 * @param y, the screen y coordinate of the slot that will be checked
 */
void updateSlot(board_t board, size_t x, size_t y, slot_t value);

/**
 * @brief returns the value the slot at the given coordinates
 * @param x, the x coordinate of the slot that will be updated
 * @param y, the y coordinate of the slot that will be updated
 */
slot_t getSlot(board_t board, size_t x, size_t y);

/**
 * @brief Checks wether or not a given position is in a base
 * @param board, the board on which we would like to check
 * @param x, the logical x-coordinate of the slot that will be checked
 * @param y, the logical y-coordinate of the slot that will be checked
 * @return 1 if the given slot is in a base, 0 otherwise and -1 in case of an error
 */
uint8_t isInBase(board_t board, size_t x, size_t y);

/**
 * @brief Clears the given board and resets it to its intial state without players or falgs
 * @param board, the board that is going to be cleared
 */
void clearBoard(board_t board);


#endif


