#ifndef __FLAG_H
#define __FLAG_H

#include "../utils.h"
#include "player.h"
#include "board.h"


#define FLAG_1_INIT_X_POS 15 * 8
#define FLAG_1_INIT_Y_POS 1 * 8
#define FLAG_2_INIT_X_POS 15 * 8 
#define FLAG_2_INIT_Y_POS 46 * 8

enum flag_type {
	FLAG_1,
	FLAG_2
};

typedef struct flag {
    size_t x;
    size_t y;
    struct player* owner;
} flag_t;

/**
 * @brief Initializes a given flag by associating it to a given player
 * @param flag, the flag that will be initialized
 * @param player, the player associated to the flag
 */
void initFlag(flag_t* flag, struct player* owner, enum flag_type ft);

/**
 * @brief Gets the given flag's logical x coordinate on the board
 * @param flag, a pointer to the flag in question
 * @return the logical x coordinate of the flag on the board
 */
uint8_t flagGetXPos(flag_t* flag);

/**
 * @brief Gets the given flag's logical y coordinate on the board
 * @param flag, a pointer to the flag in question
 * @return the logical y coordinate of the flag on the board
 */
uint8_t flagGetYPos(flag_t* flag);


#endif
