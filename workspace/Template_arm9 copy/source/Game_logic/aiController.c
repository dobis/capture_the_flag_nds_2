#include "aiController.h"

size_t manhattanDist(int x1, int y1, int x2, int y2) {
	return abs(x1 - x2) + abs(y1 - y2);
}

size_t minimax(board_t board, int x, int y, size_t depth) {
    /** TODO: Implement the minimax algorithm **/
    return 0;
}

int* nextMove(board_t board, player_t* ai) {
    /** TODO: Choose the move with the highest score according ot minimax **/
    return NULL;
}
