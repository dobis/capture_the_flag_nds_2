#include "player.h"

void initPlayer(player_t* player, struct flag* flag, enum player_type pt) {
    //Sanity check
    REQUIRE_NON_NULL(player);
    REQUIRE_NON_NULL(flag);
    REQUIRE(pt == PLAYER_1 || pt == PLAYER_2);

    //Initialize player fields
    player->flag = flag;
    player->has_flag = 0;
    player->pt = pt;

    switch (pt) {
    case PLAYER_1:
        player->x = PLAYER_1_INIT_X_POS;
        player->y = PLAYER_1_INIT_Y_POS;
        p1Cooldown = 0;
        player->dir = DOWN;
        break;

    case PLAYER_2:
        player->x = PLAYER_2_INIT_X_POS;
        player->y = PLAYER_2_INIT_Y_POS;
        p2Cooldown = 0;
        player->dir = UP;
        break;
    
    default:
        break;
    }
}

uint8_t playerGetXPos(player_t* player) {
    return player->x / BOARD_GRANULARITY;
}

uint8_t playerGetYPos(player_t* player) {
    return player->y / BOARD_GRANULARITY;
}

uint8_t checkForFlag(player_t* player, struct flag* flag) {
    //Sanity check 
    REQUIRE_NON_NULL_VAL(player, ERR);
    REQUIRE_NON_NULL_VAL(flag, ERR);

    return (playerGetXPos(player) == flagGetXPos(flag)) &&
           (playerGetYPos(player) == flagGetYPos(flag));
}  

char isHittable(player_t* hitter, player_t* hittee, enum direction hitDir) {
    //Sanity check 
    REQUIRE_NON_NULL_VAL(hitter, ERR);
    REQUIRE_NON_NULL_VAL(hittee, ERR);
    REQUIRE_VAL(hitDir == UP || hitDir == DOWN || hitDir == LEFT || hitDir == RIGHT, ERR);

    uint16_t hitterX = hitter->x + HALF_SPRITE;
    uint16_t hitterY = hitter->y + HALF_SPRITE;

    //Check the different ranges
    char lrRange = (hitterX > hittee->x && hitterX < (hittee->x + SPRITE_SIZE));
    char udRange = (hitterY > hittee->y && hitterY < (hittee->y + SPRITE_SIZE));
    char uDist = (hitter->y - HIT_DIST) < hittee->y;
    char rDist = (hitter->x + HIT_DIST) > hittee->x;
    char dDist = (hitter->y + HIT_DIST) > hittee->y;
    char lDist = (hitter->x - HIT_DIST) < hittee->x;

    //Check if the hit was a success
    switch (hitDir) {
    case UP:
        return lrRange && uDist;
    case RIGHT:
        return udRange && rDist;
    case DOWN:
        return lrRange && dDist;
    case LEFT:
        return udRange && lDist;
    default:
        break;
    }

    //If none of the directions worked, return an error 
    return ERR;
}

char hitPlayer(player_t* hitter, player_t* hittee, enum hit_type hitType, enum direction hitDir) {
    //Sanity check 
    REQUIRE_NON_NULL_VAL(hitter, ERR);
    REQUIRE_NON_NULL_VAL(hittee, ERR);
    REQUIRE_VAL(hitType == LONG_HIT || hitType == SHORT_HIT, ERR);
    REQUIRE_VAL(hitDir == UP || hitDir == DOWN || hitDir == LEFT || hitDir == RIGHT, ERR);

    if(isHittable(hitter, hittee, hitDir) == 1) {
        //Reset player's position
        switch (hittee->pt) {
            case PLAYER_1:
                //Set the hit player's cooldown 
                switch (hitType) {
                case SHORT_HIT:
                    p1Cooldown = SHORT_COOLDOWN;
                    break;
                case LONG_HIT:
                    p1Cooldown = LONG_COOLDOWN;
                default:
                    break;
                }
                break;
            case PLAYER_2:
                //Set the hit player's cooldown 
                switch (hitType) {
                case SHORT_HIT:
                    p2Cooldown = SHORT_COOLDOWN;
                    break;
                case LONG_HIT:
                    p2Cooldown = LONG_COOLDOWN;
                default:
                    break;
                }
                break;
            default:
                break;
        }
        //Have the player drop the flag
        hittee->has_flag = 0;

        //Start cooldown timer
        startHitTimer();
        return 1;
    } 
    return 0;
}
