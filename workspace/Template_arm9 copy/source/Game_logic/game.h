#ifndef __GAME_H
#define __GAME_H

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "../utils.h"
#include "board.h"
#include "player.h"
#include "flag.h"

typedef struct game game_t;

typedef enum {
    P1_VS_P2,
    P1_VS_CPU,
    CPU_VS_CPU,
    MENU
} game_mode_t;

struct game {
	board_t board;          // The current level of the game
    struct player* player1; // The first player of the game
    struct player* player2; // The second player of the game 
    game_mode_t mode;       // Game mode (who gets to play and who gats to be an AI)
    uint8_t has_winner;     // 0 if no winner yet, 1 if player1 won and 2 if player2 won
    uint8_t is_paused;
};

/**
 * @brief Initializes a given game by reseting the given board and players
 * @param game, pointer to the game that should be initialized
 * @param player1, pointer to the first player that will be playing the game
 * @param player2, pointer to the second player that will be playing the game
 * @param board, pointer to the board containing the game's state
 */
game_t* initGame(game_mode_t mode);

/**
 * @brief destructor for the game object 
 * @param game the game that will be ended
 */
void endGame(game_t* game);

/**
 * @brief verifies the game state to see if there has been a winner
 * @param game, the game that needs to be checked
 * @return true if the game has a winner, false otherwise
 */ 
char checkForWinner(game_t* game);

/**
 * @brief Checks to see if a given move will cause a collision for a given player
 * @param player the player who will be making a move
 * @param opp the opposing player with whom we might collide
 * @param moveX the amount (along the x axis) that the player will move
 * @param moveY the amount (along the y axis) that the player will move
 * @return true if a collision will happen, false otherwise and -1 in case of an error
 */
char checkForCollisions(player_t* player, player_t* opp, int8_t moveX, int8_t moveY);

/**
 * @brief Keeps the game's state up-to-date
 * @param game, pointer to the game that should be updated
 */
void updateGameState(game_t* game);

/**
 * @brief Updates the current state of the board so that it takes into account
 * the new movements of each player and their flags.
 * @param board, the board that needs to be updated
 * @param player1, the first player of the game
 * @param player2, the second player of the game
 */
void updateBoardState(board_t board, player_t* player1, player_t* player2);

/**
 * @brief Print the state of the game to the terminal
 * @param game, the game that will printed to the terminal
 */
void printState(game_t* game);

#endif

