
//{{BLOCK(controlsBottom)

//======================================================================
//
//	controlsBottom, 256x192@8, 
//	+ palette 256 entries, not compressed
//	+ 139 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 32x24 
//	Total size: 512 + 8896 + 1536 = 10944
//
//	Time-stamp: 2020-01-14, 23:00:19
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_CONTROLSBOTTOM_H
#define GRIT_CONTROLSBOTTOM_H

#define controlsBottomTilesLen 8896
extern const unsigned int controlsBottomTiles[2224];

#define controlsBottomMapLen 1536
extern const unsigned short controlsBottomMap[768];

#define controlsBottomPalLen 512
extern const unsigned short controlsBottomPal[256];

#endif // GRIT_CONTROLSBOTTOM_H

//}}BLOCK(controlsBottom)
