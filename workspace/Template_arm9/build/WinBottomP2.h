
//{{BLOCK(WinBottomP2)

//======================================================================
//
//	WinBottomP2, 256x192@8, 
//	+ palette 256 entries, not compressed
//	+ 74 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 32x24 
//	Total size: 512 + 4736 + 1536 = 6784
//
//	Time-stamp: 2020-01-14, 23:00:20
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_WINBOTTOMP2_H
#define GRIT_WINBOTTOMP2_H

#define WinBottomP2TilesLen 4736
extern const unsigned int WinBottomP2Tiles[1184];

#define WinBottomP2MapLen 1536
extern const unsigned short WinBottomP2Map[768];

#define WinBottomP2PalLen 512
extern const unsigned short WinBottomP2Pal[256];

#endif // GRIT_WINBOTTOMP2_H

//}}BLOCK(WinBottomP2)
