
//{{BLOCK(startBottom)

//======================================================================
//
//	startBottom, 256x192@8, 
//	+ palette 256 entries, not compressed
//	+ 59 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 32x24 
//	Total size: 512 + 3776 + 1536 = 5824
//
//	Time-stamp: 2020-01-14, 23:00:20
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_STARTBOTTOM_H
#define GRIT_STARTBOTTOM_H

#define startBottomTilesLen 3776
extern const unsigned int startBottomTiles[944];

#define startBottomMapLen 1536
extern const unsigned short startBottomMap[768];

#define startBottomPalLen 512
extern const unsigned short startBottomPal[256];

#endif // GRIT_STARTBOTTOM_H

//}}BLOCK(startBottom)
