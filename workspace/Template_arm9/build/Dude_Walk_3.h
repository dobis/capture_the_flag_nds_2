
//{{BLOCK(Dude_Walk_3)

//======================================================================
//
//	Dude_Walk_3, 96x32@4, 
//	Transparent color : 00,00,00
//	+ palette 16 entries, not compressed
//	+ 48 tiles not compressed
//	Total size: 32 + 1536 = 1568
//
//	Time-stamp: 2020-01-14, 23:00:19
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_DUDE_WALK_3_H
#define GRIT_DUDE_WALK_3_H

#define Dude_Walk_3TilesLen 1536
extern const unsigned int Dude_Walk_3Tiles[384];

#define Dude_Walk_3PalLen 32
extern const unsigned short Dude_Walk_3Pal[16];

#endif // GRIT_DUDE_WALK_3_H

//}}BLOCK(Dude_Walk_3)
