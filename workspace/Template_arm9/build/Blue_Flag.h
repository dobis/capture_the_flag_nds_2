
//{{BLOCK(Blue_Flag)

//======================================================================
//
//	Blue_Flag, 32x32@4, 
//	Transparent color : 00,00,00
//	+ palette 16 entries, not compressed
//	+ 16 tiles not compressed
//	Total size: 32 + 512 = 544
//
//	Time-stamp: 2020-01-14, 23:00:19
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_BLUE_FLAG_H
#define GRIT_BLUE_FLAG_H

#define Blue_FlagTilesLen 512
extern const unsigned int Blue_FlagTiles[128];

#define Blue_FlagPalLen 32
extern const unsigned short Blue_FlagPal[16];

#endif // GRIT_BLUE_FLAG_H

//}}BLOCK(Blue_Flag)
