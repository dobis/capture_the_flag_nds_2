#include <nds.h>

#include "controls.h"


int keys = 0 ;
int keys_pressed = 0;

void handleInput(game_t* game) {

	scanKeys();
	keys = keysHeld();
	
	keys_pressed = keysDown();

	if(!(game->is_paused)) {

		//Check for pause
		if(keys_pressed & KEY_START) {
			game->is_paused = 1;
		}

		//Check that the player is allowed to move
		if(p1Cooldown == 0) {

			//Update Player 1 position and attack
			if(keys & KEY_L) {
				if(hitPlayer(game->player1, game->player2, SHORT_HIT, game->player1->dir) != 0){
					oamClear(&oamSub, 2,1 ); // clear flag 1 in bottom screen
				}
				soundPlaySoundFX(SFX_CLUNK);
			}

			if((keys & KEY_RIGHT) && (game->player1->x < (SCREEN_WIDTH - SPRITE_WIDTH))) {
				if(!checkForCollisions(game->player1, game->player2, 2, 0)) {
					game->player1->x += 2;
				}
				game->player1->dir = RIGHT;
			}
			if((keys & KEY_DOWN) && (game->player1->y <= (SCREEN_HEIGHT - SPRITE_HEIGHT))) {
				game->player1->sT = P_TOP;
				if(!checkForCollisions(game->player1, game->player2, 0, 2)) {
					game->player1->y += 2;
				}
				game->player1->dir = DOWN;
			}
			if((keys & KEY_DOWN) && ((game->player1->y >= (SCREEN_HEIGHT + SPRITE_HEIGHT) ) && (game->player1->y< (2*SCREEN_HEIGHT - SPRITE_HEIGHT)))) {
				game->player1->sT = P_BOTTOM;
				if(!checkForCollisions(game->player1, game->player2, 0, 2)) {
					game->player1->y += 2;
				}
				game->player1->dir = DOWN;
			}
			if((keys & KEY_DOWN) && ((game->player1->y > (SCREEN_HEIGHT - SPRITE_HEIGHT) ) && (game->player1->y< (SCREEN_HEIGHT + SPRITE_HEIGHT)))) {
				game->player1->sT = P_BOTH;
				if(!checkForCollisions(game->player1, game->player2, 0, 2)) {
					game->player1->y += 2;
				}
				game->player1->dir = DOWN;
			}
			if((keys & KEY_LEFT) && (game->player1->x  > 0)) {
				if(!checkForCollisions(game->player1, game->player2, -2, 0)) {
					game->player1->x -= 2;
				}
				game->player1->dir = LEFT;
			}
			
			if((keys & KEY_UP) && (game->player1->y > 0) && (game->player1->y<= (SCREEN_HEIGHT - SPRITE_HEIGHT) )) {
				game->player1->sT = P_TOP;
				if(!checkForCollisions(game->player1, game->player2, 0, -2)) {
					game->player1->y -= 2;
				}
				game->player1->dir = UP;
			}
			if((keys & KEY_UP) && (game->player1->y > 0) && (game->player1->y>= (SCREEN_HEIGHT + SPRITE_HEIGHT)) ) {
				game->player1->sT = P_BOTTOM;
				if(!checkForCollisions(game->player1, game->player2, 0, -2)) {
					game->player1->y -= 2;
				}
				game->player1->dir = UP;

			}
			if((keys & KEY_UP) && (game->player1->y > (SCREEN_HEIGHT - SPRITE_HEIGHT) ) && (game->player1->y< (SCREEN_HEIGHT + SPRITE_HEIGHT))) {
				game->player1->sT= P_BOTH;
				if(!checkForCollisions(game->player1, game->player2, 0, -2)) {
					game->player1->y -= 2;
				}
				game->player1->dir = UP;

			}
		}

		//Check that the player is allowed to move
		if(p2Cooldown == 0) {

			if(keys & KEY_R) {
				if(hitPlayer(game->player2, game->player1, SHORT_HIT, game->player2->dir) != 0){
					oamClear(&oamMain, 3, 1); //clear flag 2 in main screen
				}
				soundPlaySoundFX(SFX_SWISH);
			}

			if((keys & KEY_A) && (game->player2->x < (SCREEN_WIDTH - SPRITE_WIDTH))) {
				if(!checkForCollisions(game->player2, game->player1, 2, 0)) {
					game->player2->x += 2;
				}
				game->player2->dir = RIGHT;
			}
			if((keys & KEY_B) && (game->player2->y <= (SCREEN_HEIGHT - SPRITE_HEIGHT))) {
				game->player2->sT = P_TOP;
				if(!checkForCollisions(game->player2, game->player1, 0, 2)) {
					game->player2->y += 2;
				}
				game->player2->dir = DOWN;
			}
			if((keys & KEY_B) && (game->player2->y >= (SCREEN_HEIGHT + SPRITE_HEIGHT)) && (game->player2->y < (2*SCREEN_HEIGHT - SPRITE_HEIGHT))) {
				game->player2->sT = P_BOTTOM;
				if(!checkForCollisions(game->player2, game->player1, 0, 2)) {
					game->player2->y += 2;
				}
				game->player2->dir = DOWN;
			}
			if((keys & KEY_B) && (game->player2->y > (SCREEN_HEIGHT- SPRITE_HEIGHT) && (game->player2->y < (SCREEN_HEIGHT + SPRITE_HEIGHT)))) {
				game->player2->sT = P_BOTH;
				if(!checkForCollisions(game->player2, game->player1, 0, 2)) {
					game->player2->y += 2;
				}
				game->player2->dir = DOWN;
			}
			if((keys & KEY_Y) && (game->player2->x  > 0)) {
				if(!checkForCollisions(game->player2, game->player1, -2, 0)) {
					game->player2->x -= 2;
				}
				game->player2->dir = LEFT;
			}
			if((keys & KEY_X) && (game->player2->y  > 0) && (game->player2->y <= (SCREEN_HEIGHT - SPRITE_HEIGHT) )) {
				game->player2->sT = P_TOP;
				if(!checkForCollisions(game->player2, game->player1, 0, -2)) {
					game->player2->y -= 2;
				}
				game->player2->dir = UP;
			}
			if((keys & KEY_X) && (game->player2->y  > 0) && (game->player2->y >= (SCREEN_HEIGHT + SPRITE_HEIGHT))) {
				game->player2->sT = P_BOTTOM;
				if(!checkForCollisions(game->player2, game->player1, 0, -2)) {
					game->player2->y -=2;
				}
				game->player2->dir = UP;
			}
			if((keys & KEY_X) && (game->player2->y  > (SCREEN_HEIGHT - SPRITE_HEIGHT)) && (game->player2->y < (SCREEN_HEIGHT + SPRITE_HEIGHT))) {
				game->player2->sT = P_BOTH;
				if(!checkForCollisions(game->player2, game->player1, 0, -2)) {
					game->player2->y -= 2;
				}
				game->player2->dir = UP;

			}
		}

		//Update Flag 1 position (flag 1 belongs to player 1 and is initialized in in player 1 base ( NOT on other side of arena))
		if(game->player1->has_flag) {

			game->player2->flag->x = game->player1->x;
			game->player2->flag->y = game->player1->y;
			game->player2->flag->sT = game->player1->sT;
		}

		//Update Flag 2
		if(game->player2->has_flag) {

			game->player1->flag->x = game->player2->x;
			game->player1->flag->y = game->player2->y;
			game->player1->flag->sT = game->player2->sT;
		}
	}

	//Display sprites
	showSprites(game);


}

void handleTouch(enum TOUCH_TYPE* result) {

	touchPosition touch;
	touchRead(&touch);

	//Read the input from the touchScreen if there is one
	if(touch.px || touch.py){

		if(touch.px < HALF_SCREEN) {
			*result = PLAY;
		} else if(touch.px > HALF_SCREEN) {
			*result = STOP;
		}
	}
}
