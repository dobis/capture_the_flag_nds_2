#ifndef __PLAYER_H
#define __PLAYER_H

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "../utils.h"
#include "../timer.h"
#include "flag.h"

#define SPRITE_SIZE 32
#define HALF_SPRITE 16
#define HIT_DIST 16
#define SHORT_COOLDOWN 10
#define LONG_COOLDOWN 30
#define PLAYER_1_INIT_X_POS 15 * 8
#define PLAYER_1_INIT_Y_POS 1 * 8
#define PLAYER_2_INIT_X_POS 15 * 8
#define PLAYER_2_INIT_Y_POS 43 * 8

enum player_type {
	PLAYER_1,
	PLAYER_2,
    NO_WIN
};

enum hit_type {
    LONG_HIT,
    SHORT_HIT
};

enum direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
};

enum player_screen_type {
	P_TOP,
	P_BOTTOM,
	P_BOTH
};

typedef struct player {
    size_t x;
    size_t y;
    struct flag* flag;
    uint8_t has_flag;
    enum player_type pt;
    enum direction dir;
    enum player_screen_type sT;

} player_t;

/**
 * @brief Initializes a given player using the given flag 
 * @param player, a pointer to the player that will be initialized
 * @param flag, a pointer to the flag associated to the player
 */ 
void initPlayer(player_t* player, struct flag* flag, enum player_type pt);

/**
 * @brief Gets the given player's logical x coordinate on the board
 * @param player, a pointer to the player in question
 * @return the logical x coordinate of the player on the board
 */
uint8_t playerGetXPos(player_t* player);

/**
 * @brief Gets the given player's logical y coordinate on the board
 * @param player, a pointer to the player in question
 * @return the logical y coordinate of the player on the board
 */
uint8_t playerGetYPos(player_t* player);

/**
 * @brief Checks to see if the player has acquired the adversary's flag 
 * @param player, the player we want to check for 
 * @param flag, the adversary's flag that is going to be captured
 * @return 1 if the player has captured the flag, 0 otherwise and -1 in case of an error
 */
uint8_t checkForFlag(player_t* player, struct flag* flag);

/**
 * @brief Perfoms a hit, from a hitter to a hittee 
 * @param hitter, the player that is performing the hit
 * @param hittee, the player that is being hit
 * @param hitType, whether the hit was long or short
 * @param hitDir, the direction in which the hit was performed
 * @return true if the hit was a success, false otherwise and ERR in case of an error
 */ 
int hitPlayer(player_t* hitter, player_t* hittee, enum hit_type hitType, enum direction hitDir);

/**
 * @brief Checks whether or not a given player is hittable by an other player
 * @param hitter, the player that is performing the hit
 * @param hittee, the player that is being hit
 * @param dir, the direction in which the hit was performed
 * @return true if the player is hittable and false otherwise, ERR is case of an error
 */
int isHittable(player_t* hitter, player_t* hittee, enum direction hitDir);

/**
 * @brief resets player after scoring
 * @param player, the player we want to reset
 */
void resetPlayer(player_t* player);


#endif


