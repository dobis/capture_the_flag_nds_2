#pragma once
#include <nds.h>

#include "Game_logic/game.h"
#include "utils.h"

#define SPRITE_HEIGHT 32
#define SPRITE_WIDTH 32
#define SCREEN_HEIGHT 192
#define SCREEN_WIDTH 256

u16* gfx_p1;
u16* gfx_p1_sub;
u16* gfx_p2;
u16* gfx_p2_sub;
u16* gfx_f1;
u16* gfx_f1_sub;
u16* gfx_f2;
u16* gfx_f2_sub;


enum BUFFER_TYPE
{
    MAIN,
    SUB
};


enum SPRITE_NUMBER
{
    PLAYER1,
    PLAYER2,
    FLAG1,
    FLAG2
};

/**
 * @brief This function sets up the main graphical engine to work in MODE 5 and activates 2 BGs (1 and 3).
 * It also sets up the corresponding VRAM bank
 */
void configureGraphics_Main();

/**
 *  @brief Configures the SUB engine in the same way as and activates BG3 and BG1. 
 *  corresponding VRAM bank
 */
void configureGraphics_Sub();

/**
 * 	@brief Configures the background BG3 in extended rotoscale mode using the 16-bit pixel mode (i.e. emulating framebuffer mode)
 * Configures the background BG1 in TILED mode. (BG0 will be kept for foreground elements later on)
 */
void configureBG(enum BUFFER_TYPE bT);

/**
 * @brief Copies data to replace BG with the menu BG
 */ 
void showMenu();

/**
 * @brief Copies the data necessary to show the starting screen
 */ 
void showStart();

/**
 * @brief Copies the data necessary to show the controls screen
 */
void showControls();

/**
 * @brief Shows the game's BG on a given engine
 */ 
void showBG(enum BUFFER_TYPE bT);

/**
 * @brief Shows the winner bg associated to a given winner
 * @param pt the player that has won
 */ 
void showWin(enum SPRITE_NUMBER pt);

/**
 * @brief resets the game's graphics to the initial state
 */
void resetGraphics();

/**
 * @brief configures the sprite engine
 */
void configureSprites();

/**
 * @brief updates the sprites locations
 */
void showSprites(game_t* game);

