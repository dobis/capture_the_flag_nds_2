/*
 * Capture the flag Nintendo DS
 */

#include <nds.h>
#include <stdio.h>
#include "utils.h"
#include "Game_logic/game.h"
#include "graphics.h"
#include "controls.h"
#include "sound.h"
#include "timer.h"

void wait_f(int i) {
    setWait(i);
    startWaitTimer();
    while(getWait() != 0);
}

int main(void) {
    //Setup Graphics
    configureGraphics_Main();
    configureGraphics_Sub();

    //Initialize the timer
    initTimer();

    //Initialize music
    soundInit();

    //Inititalize game
    game_t* game = initGame(P1_VS_P2);

    //Play music
    soundPlayMusic();

    //Setup  backgrounds
    configureBG(MAIN);
    configureBG(SUB);   
    
    //Setup Sprites
    configureSprites();

    //flag to know when to copy back the old BG and touchscreen postition
    char was_paused = 0;
    char has_started = 0;

    enum TOUCH_TYPE tt = NO_PAUSE;

    //Show the starting screen
    showStart();

    //Starting loop
    while(!has_started) {
        handleTouch(&tt);
        if(tt != NO_PAUSE) {
            has_started = 1;
        }
    }

    wait_f(2);

    tt = NO_PAUSE;

    //Controls loop
    showControls();

    while (1) {
        handleTouch(&tt);
        if(tt != NO_PAUSE) {
            break;
        }
    }

    wait_f(2);

    tt = NO_PAUSE;

    //Reset the graphics before the main loop
    resetGraphics();

    //Main gameplay loop
    while(1) {
        
        if(game->is_paused) {
            handleTouch(&tt);
            //If stop return to the starting screen
            if(tt == STOP) {
            	oamClear(&oamMain, 0, 0);
            	oamClear(&oamSub, 0, 0);
                oamUpdate(&oamMain);
                oamUpdate(&oamSub);

            	wait_f(2);

                game->is_paused = 0;
                has_started = 0;
                //Show the starting screen
                showStart();

                //Starting loop
                while(!has_started) {
                    handleTouch(&tt);
                    if(tt != NO_PAUSE) {
                        has_started = 1;
                    }
                }

                //wait for 2s
                wait_f(2);

               tt = NO_PAUSE;

                //Controls loop
                showControls();

                while (1) {
                    handleTouch(&tt);
                    if(tt != NO_PAUSE) {
                        break;
                    }
                }

                //wait for 2s
                wait_f(2);

                tt = NO_PAUSE;

                //Reset the graphics before going back to the main loop
                resetGraphics();
                resetGame(game);

            } else if(tt == PLAY) {
                game->is_paused = 0;
            }
        } else if(game->has_winner != 0) {
            //Decide which winning screen to show and wait for input
            showWin(game->winner);
            handleTouch(&tt);

            //If the screen was hit then continue playing
            if(tt != NO_PAUSE) {
                game->winner = NO_WIN;
                game->has_winner = 0;
                tt = NO_PAUSE;

                //reset the game to the initial state
                resetGame(game);
                resetGraphics();
            }
        } else {
            //Read held keys
    	    handleInput(game);
    	    tt = NO_PAUSE;

			//Update the game's logical state
			updateGameState(game);
        }
        
        //Check to see when the game was paused
        if(game->is_paused && !was_paused) {
            showMenu();
            was_paused = 1;

        }else if(!(game->is_paused) && was_paused) {
            showBG(SUB);
            was_paused = 0;
        }



    	swiWaitForVBlank();

        oamUpdate(&oamMain);
        oamUpdate(&oamSub);
    }

    //End the game and return 
    endGame(game);
    return ERR_NONE;
}
