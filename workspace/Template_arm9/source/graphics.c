#include "graphics.h"
#include "bg.h"
#include "bg_SUB.h"
#include "Pink_Monster.h"
#include "Dude_Monster.h"
#include "Pink_Flag.h"
#include "Blue_Flag.h"
#include "menu.h"
#include "WinTop.h"
#include "WinBottomP1.h"
#include "WinBottomP2.h"
#include "start.h"
#include "startBottom.h"
#include "controlsTop.h"
#include "controlsBottom.h"
#include <string.h>

void configureGraphics_Main() {
    //Setup in mode 5 with BG1 and BG3 active
    REG_DISPCNT = MODE_5_2D |  DISPLAY_BG0_ACTIVE;

    //Setup VRAM
    VRAM_A_CR = VRAM_ENABLE | VRAM_A_MAIN_BG;
}

/**
 *  @brief Configures the SUB engine in the same way as and activates BG3 and BG1. 
 *  corresponding VRAM bank
 */
void configureGraphics_Sub() {
    //Setup in mode 5 with BG1 and BG3 active
    REG_DISPCNT_SUB = MODE_5_2D | DISPLAY_BG0_ACTIVE;

    //Setup VRAM
    VRAM_C_CR = VRAM_ENABLE | VRAM_C_SUB_BG;
}

void showBG(enum BUFFER_TYPE bT) {

	switch(bT) {
		case MAIN:
			//Affine Matrix Transformation
            REG_BG2PA = 256;
            REG_BG2PC = 0;
            REG_BG2PB = 0;
            REG_BG2PD = 256;

            //Copy background to memory
            swiCopy(bgPal, BG_PALETTE, bgPalLen/2);
            swiCopy(bgMap, BG_MAP_RAM(0), bgMapLen/2);
            swiCopy(bgTiles, BG_TILE_RAM(1), bgTilesLen/2);
			break;
		case SUB:
			//Affine Matrix Transformation
            REG_BG2PA_SUB = 256;
            REG_BG2PC_SUB = 0;
            REG_BG2PB_SUB = 0;
            REG_BG2PD_SUB = 256;

            //Copy background to memory
            swiCopy(bg_SUBPal, BG_PALETTE_SUB, bg_SUBPalLen/2);
            swiCopy(bg_SUBMap, BG_MAP_RAM_SUB(0), bg_SUBMapLen/2);
            swiCopy(bg_SUBTiles, BG_TILE_RAM_SUB(1), bg_SUBTilesLen/2);
            break;
	}
	
}

/**
 * 	@brief Configures the background BG3 in extended rotoscale mode using the 16-bit pixel mode (i.e. emulating framebuffer mode)
 * Configures the background BG1 in TILED mode. (BG0 will be kept for foreground elements later on)
 */
void configureBG(enum BUFFER_TYPE bT) {
    switch(bT) {
        case MAIN:
            //Initialize backgrounds
        	BGCTRL[0] = BG_COLOR_256 | BG_MAP_BASE(0) | BG_TILE_BASE(1) | BG_32x32;
			showBG(MAIN);
            break;
        case SUB:
            //Initialize backgrounds
            BGCTRL_SUB[0] = BG_COLOR_256 | BG_MAP_BASE(0) | BG_TILE_BASE(1) | BG_32x32;
			showBG(SUB);
			break;
    }
}

void showMenu() {
	//Affine Matrix Transformation
	REG_BG2PA_SUB = 256;
	REG_BG2PC_SUB = 0;
	REG_BG2PB_SUB = 0;
	REG_BG2PD_SUB = 256;

	//Copy background to memory
	swiCopy(menuPal, BG_PALETTE_SUB, menuPalLen/2);
	swiCopy(menuMap, BG_MAP_RAM_SUB(0), menuMapLen/2);
	swiCopy(menuTiles, BG_TILE_RAM_SUB(1), menuTilesLen/2);
}

void showStart() {
	//Affine Matrix Transformation for the top screen
	REG_BG2PA = 256;
	REG_BG2PC = 0;
	REG_BG2PB = 0;
	REG_BG2PD = 256;

	//Affine Matrix Transformation for the bottom screen
	REG_BG2PA_SUB = 256;
	REG_BG2PC_SUB = 0;
	REG_BG2PB_SUB = 0;
	REG_BG2PD_SUB = 256;

	//Copy background to memory
	swiCopy(startPal, BG_PALETTE, startPalLen/2);
	swiCopy(startMap, BG_MAP_RAM(0), startMapLen/2);
	swiCopy(startTiles, BG_TILE_RAM(1), startTilesLen/2);

	//Copy background to memory
	swiCopy(startBottomPal, BG_PALETTE_SUB, startBottomPalLen/2);
	swiCopy(startBottomMap, BG_MAP_RAM_SUB(0), startBottomMapLen/2);
	swiCopy(startBottomTiles, BG_TILE_RAM_SUB(1), startBottomTilesLen/2);
}

void showControls() {
	//Affine Matrix Transformation for the top screen
	REG_BG2PA = 256;
	REG_BG2PC = 0;
	REG_BG2PB = 0;
	REG_BG2PD = 256;

	//Affine Matrix Transformation for the bottom screen
	REG_BG2PA_SUB = 256;
	REG_BG2PC_SUB = 0;
	REG_BG2PB_SUB = 0;
	REG_BG2PD_SUB = 256;

	//Copy background to memory
	swiCopy(controlsTopPal, BG_PALETTE, controlsTopPalLen/2);
	swiCopy(controlsTopMap, BG_MAP_RAM(0), controlsTopMapLen/2);
	swiCopy(controlsTopTiles, BG_TILE_RAM(1), controlsTopTilesLen/2);

	//Copy background to memory
	swiCopy(controlsBottomPal, BG_PALETTE_SUB, controlsBottomPalLen/2);
	swiCopy(controlsBottomMap, BG_MAP_RAM_SUB(0), controlsBottomMapLen/2);
	swiCopy(controlsBottomTiles, BG_TILE_RAM_SUB(1), controlsBottomTilesLen/2);
}

void showWin(enum SPRITE_NUMBER pt) {
	//Sanity check
	REQUIRE(pt == PLAYER1 || pt == PLAYER2);

	//Show top screen
	REG_BG2PA = 256;
	REG_BG2PC = 0;
	REG_BG2PB = 0;
	REG_BG2PD = 256;

	//Affine Matrix Transformation
	REG_BG2PA_SUB = 256;
	REG_BG2PC_SUB = 0;
	REG_BG2PB_SUB = 0;
	REG_BG2PD_SUB = 256;

	//Copy background to memory
	swiCopy(WinTopPal, BG_PALETTE, WinTopPalLen/2);
	swiCopy(WinTopMap, BG_MAP_RAM(0), WinTopMapLen/2);
	swiCopy(WinTopTiles, BG_TILE_RAM(1), WinTopTilesLen/2);
	
	//Show bottom screen
	switch (pt) {
	case PLAYER1:

		//Copy P1 win background to memory
		swiCopy(WinBottomP1Pal, BG_PALETTE_SUB, WinBottomP1PalLen/2);
		swiCopy(WinBottomP1Map, BG_MAP_RAM_SUB(0), WinBottomP1MapLen/2);
		swiCopy(WinBottomP1Tiles, BG_TILE_RAM_SUB(1), WinBottomP1TilesLen/2);

		break;
	case PLAYER_2:

		//Copy P2 win background to memory
		swiCopy(WinBottomP2Pal, BG_PALETTE_SUB, WinBottomP2PalLen/2);
		swiCopy(WinBottomP2Map, BG_MAP_RAM_SUB(0), WinBottomP2MapLen/2);
		swiCopy(WinBottomP2Tiles, BG_TILE_RAM_SUB(1), WinBottomP2TilesLen/2);

		break;
	default:
		break;
	}
}

void resetGraphics() {
	//Update backgrounds
	showBG(MAIN);
	showBG(SUB);

	//Clear Sprites
	oamClear(&oamMain, 0, 4);
	oamClear(&oamSub, 0, 4);

}


void configureSprites() {
	//Set up memory bank to work in sprite mode (offset since we are using VRAM A for backgrounds)
	VRAM_B_CR = VRAM_ENABLE | VRAM_B_MAIN_SPRITE_0x06400000;

	//Set up VRAM for Sub engine
	VRAM_D_CR = VRAM_ENABLE | VRAM_D_SUB_SPRITE;

	//Initialize sprite manager and the engine
	oamInit(&oamMain, SpriteMapping_1D_32, false);  //changed 1D_32 to 1D_128
	oamInit(&oamSub, SpriteMapping_1D_32, false);

	//Allocate space for the graphic to show in the sprite
	// Players
	gfx_p1 = oamAllocateGfx(&oamMain, SpriteSize_32x32, SpriteColorFormat_16Color);
	gfx_p1_sub = oamAllocateGfx(&oamSub, SpriteSize_32x32, SpriteColorFormat_16Color);

	gfx_p2 = oamAllocateGfx(&oamMain, SpriteSize_32x32, SpriteColorFormat_16Color);
	gfx_p2_sub = oamAllocateGfx(&oamSub, SpriteSize_32x32, SpriteColorFormat_16Color);


	//Flags
	gfx_f1 = oamAllocateGfx(&oamMain, SpriteSize_32x32, SpriteColorFormat_16Color);
	gfx_f1_sub = oamAllocateGfx(&oamSub, SpriteSize_32x32, SpriteColorFormat_16Color);

	gfx_f2 = oamAllocateGfx(&oamMain, SpriteSize_32x32, SpriteColorFormat_16Color);
	gfx_f2_sub = oamAllocateGfx(&oamSub, SpriteSize_32x32, SpriteColorFormat_16Color);

	
	//Copy data for the graphic (palette and tiles)
	//Sprite Main - Player 1
	swiCopy(Pink_MonsterPal, &SPRITE_PALETTE[0], Pink_MonsterPalLen/2);
	swiCopy(Pink_MonsterTiles, gfx_p1, Pink_MonsterTilesLen/2);

	//Sprite Sub - Player 1
	swiCopy(Pink_MonsterPal, &SPRITE_PALETTE_SUB[0], Pink_MonsterPalLen/2);
	swiCopy(Pink_MonsterTiles, gfx_p1_sub, Pink_MonsterTilesLen/2);

	//Sprite Main - Player 2
	swiCopy(Dude_MonsterPal, SPRITE_PALETTE + Pink_MonsterPalLen/2, Dude_MonsterPalLen/2);
	swiCopy(Dude_MonsterTiles, gfx_p2 , Dude_MonsterTilesLen/2);

	//Sprite Sub - Player 2
	swiCopy(Dude_MonsterPal, SPRITE_PALETTE_SUB + Pink_MonsterPalLen/2 , Dude_MonsterPalLen/2);
	swiCopy(Dude_MonsterTiles, gfx_p2_sub , Dude_MonsterTilesLen/2);

	//Sprite Main - Flag 1
	swiCopy(Pink_FlagPal, SPRITE_PALETTE + Pink_MonsterPalLen/2 + Dude_MonsterPalLen/2, Pink_FlagPalLen/2);
	swiCopy(Pink_FlagTiles, gfx_f1 , Pink_FlagTilesLen/2);

	//Sprite Sub - Flag 1
	swiCopy(Pink_FlagPal, SPRITE_PALETTE_SUB + Pink_MonsterPalLen/2 + Dude_MonsterPalLen/2, Pink_FlagPalLen/2);
	swiCopy(Pink_FlagTiles, gfx_f1_sub , Pink_FlagTilesLen/2);


	//Sprite Main - Flag 2
	swiCopy(Blue_FlagPal, SPRITE_PALETTE + Pink_MonsterPalLen/2 + Dude_MonsterPalLen/2 + Pink_FlagPalLen/2, Blue_FlagPalLen/2);
	swiCopy(Blue_FlagTiles, gfx_f2 , Blue_FlagTilesLen/2);

	//Sprite Sub - Flag 2
	swiCopy(Blue_FlagPal, SPRITE_PALETTE_SUB + Pink_MonsterPalLen/2 + Dude_MonsterPalLen/2 + Pink_FlagPalLen/2, Blue_FlagPalLen/2);
	swiCopy(Blue_FlagTiles, gfx_f2_sub , Blue_FlagTilesLen/2);


}


void showSprites(game_t* game){

	//PLAYER 1
	switch(game->player1->sT){
		case P_TOP :
			oamSet(&oamMain, 	// oam handler
					0,				// Number of sprite
					game->player1->x, game->player1->y,			// Coordinates
					0,				// Priority
					0,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_p1,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case P_BOTTOM :
			oamSet(&oamSub, 	// oam handler
					0,				// Number of sprite
					game->player1->x, game->player1->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					0,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_p1_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case P_BOTH :
			oamSet(&oamMain, 	// oam handler
					0,				// Number of sprite
					game->player1->x, game->player1->y,			// Coordinates
					0,				// Priority
					0,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_p1,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			oamSet(&oamSub, 	// oam handler
					0,				// Number of sprite
					game->player1->x, game->player1->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					0,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_p1_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;
	}

	//PLAYER 2

	switch(game->player2->sT){
		case P_TOP :
			oamSet(&oamMain, 	// oam handler
					1,				// Number of sprite
					game->player2->x, game->player2->y,			// Coordinates
					0,				// Priority
					1,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_p2,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
					);
			break;

		case P_BOTTOM :
			oamSet(&oamSub, 	// oam handler
					1,				// Number of sprite
					game->player2->x, game->player2->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					1,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_p2_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
					);
			break;

		case P_BOTH :
			oamSet(&oamMain, 	// oam handler
					1,				// Number of sprite
					game->player2->x, game->player2->y,			// Coordinates
					0,				// Priority
					1,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_p2,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
					);
			oamSet(&oamSub, 	// oam handler
					1,				// Number of sprite
					game->player2->x, game->player2->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					1,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_p2_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
					);
			break;
	}

	//FLAG 1
	switch(game->player1->flag->sT){
		case F_TOP :
			oamSet(&oamMain, 	// oam handler
					2,				// Number of sprite
					game->player1->flag->x, game->player1->flag->y,  //game->player1->flag->x, game->player1->flag->y,			// Coordinates
					0,				// Priority
					2,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f1,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case F_BOTTOM :
			oamSet(&oamSub, 	// oam handler
					2,				// Number of sprite
					game->player1->flag->x, game->player1->flag->y - SCREEN_HEIGHT, //game->player1->flag->x, game->player1->flag->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					2,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f1_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case F_BOTH :
			oamSet(&oamMain, 	// oam handler
					2,				// Number of sprite
					game->player1->flag->x, game->player1->flag->y,			// Coordinates
					0,				// Priority
					2,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f1,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			oamSet(&oamSub, 	// oam handler
					2,				// Number of sprite
					game->player1->flag->x, game->player1->flag->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					2,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f1_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;
	}

	//FLAG 2

	switch(game->player2->flag->sT){
		case F_TOP :
			oamSet(&oamMain, 	// oam handler
					3,				// Number of sprite
					game->player2->flag->x, game->player2->flag->y, //game->player2->flag->x, game->player2->flag->y,			// Coordinates
					0,				// Priority
					3,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f2,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case F_BOTTOM :
			oamSet(&oamSub, 	// oam handler
					3,				// Number of sprite
					game->player2->flag->x, game->player2->flag->y - SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					3,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f2_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

		case F_BOTH :
			oamSet(&oamMain, 	// oam handler
					3,				// Number of sprite
					game->player2->flag->x, game->player2->flag->y,			// Coordinates
					0,				// Priority
					3,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f2,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			oamSet(&oamSub, 	// oam handler
					3,				// Number of sprite
					game->player2->flag->x, game->player2->flag->y -  SCREEN_HEIGHT,			// Coordinates
					0,				// Priority
					3,				// Palette to use
					SpriteSize_32x32,			// Sprite size
					SpriteColorFormat_16Color,	// Color format
					gfx_f2_sub,			// Loaded graphic to display
					-1,				// Affine rotation to use (-1 none)
					false,			// Double size if rotating
					false,			// Hide this sprite
					false, false,	// Horizontal or vertical flip
					false			// Mosaic
				);
			break;

	}



}
