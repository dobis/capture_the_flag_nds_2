
//{{BLOCK(WinBottomP1)

//======================================================================
//
//	WinBottomP1, 256x192@8, 
//	+ palette 256 entries, not compressed
//	+ 68 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 32x24 
//	Total size: 512 + 4352 + 1536 = 6400
//
//	Time-stamp: 2020-01-14, 23:29:20
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_WINBOTTOMP1_H
#define GRIT_WINBOTTOMP1_H

#define WinBottomP1TilesLen 4352
extern const unsigned int WinBottomP1Tiles[1088];

#define WinBottomP1MapLen 1536
extern const unsigned short WinBottomP1Map[768];

#define WinBottomP1PalLen 512
extern const unsigned short WinBottomP1Pal[256];

#endif // GRIT_WINBOTTOMP1_H

//}}BLOCK(WinBottomP1)
