
//{{BLOCK(Pink_Monster)

//======================================================================
//
//	Pink_Monster, 32x32@4, 
//	Transparent color : 00,00,00
//	+ palette 16 entries, not compressed
//	+ 16 tiles not compressed
//	Total size: 32 + 512 = 544
//
//	Time-stamp: 2020-01-14, 23:29:19
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_PINK_MONSTER_H
#define GRIT_PINK_MONSTER_H

#define Pink_MonsterTilesLen 512
extern const unsigned int Pink_MonsterTiles[128];

#define Pink_MonsterPalLen 32
extern const unsigned short Pink_MonsterPal[16];

#endif // GRIT_PINK_MONSTER_H

//}}BLOCK(Pink_Monster)
