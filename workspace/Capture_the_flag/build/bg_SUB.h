
//{{BLOCK(bg_SUB)

//======================================================================
//
//	bg_SUB, 256x192@8, 
//	+ palette 256 entries, not compressed
//	+ 5 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 32x24 
//	Total size: 512 + 320 + 1536 = 2368
//
//	Time-stamp: 2020-01-14, 23:29:19
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_BG_SUB_H
#define GRIT_BG_SUB_H

#define bg_SUBTilesLen 320
extern const unsigned int bg_SUBTiles[80];

#define bg_SUBMapLen 1536
extern const unsigned short bg_SUBMap[768];

#define bg_SUBPalLen 512
extern const unsigned short bg_SUBPal[256];

#endif // GRIT_BG_SUB_H

//}}BLOCK(bg_SUB)
