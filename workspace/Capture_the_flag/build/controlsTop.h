
//{{BLOCK(controlsTop)

//======================================================================
//
//	controlsTop, 256x192@8, 
//	+ palette 256 entries, not compressed
//	+ 137 tiles (t|f reduced) not compressed
//	+ regular map (flat), not compressed, 32x24 
//	Total size: 512 + 8768 + 1536 = 10816
//
//	Time-stamp: 2020-01-14, 23:29:19
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_CONTROLSTOP_H
#define GRIT_CONTROLSTOP_H

#define controlsTopTilesLen 8768
extern const unsigned int controlsTopTiles[2192];

#define controlsTopMapLen 1536
extern const unsigned short controlsTopMap[768];

#define controlsTopPalLen 512
extern const unsigned short controlsTopPal[256];

#endif // GRIT_CONTROLSTOP_H

//}}BLOCK(controlsTop)
