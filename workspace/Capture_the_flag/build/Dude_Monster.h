
//{{BLOCK(Dude_Monster)

//======================================================================
//
//	Dude_Monster, 32x32@4, 
//	Transparent color : 00,00,00
//	+ palette 16 entries, not compressed
//	+ 16 tiles not compressed
//	Total size: 32 + 512 = 544
//
//	Time-stamp: 2020-01-14, 23:29:19
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.3
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_DUDE_MONSTER_H
#define GRIT_DUDE_MONSTER_H

#define Dude_MonsterTilesLen 512
extern const unsigned int Dude_MonsterTiles[128];

#define Dude_MonsterPalLen 32
extern const unsigned short Dude_MonsterPal[16];

#endif // GRIT_DUDE_MONSTER_H

//}}BLOCK(Dude_Monster)
