#include "timer.h"

int wait = 0;

void initTimer() {
    //Initialize timer
	TIMER0_DATA = TIMER_FREQ_1024(10);
    TIMER0_CR = TIMER_ENABLE | TIMER_DIV_1024 | TIMER_IRQ_REQ ;

    //Initialize timer 1
    TIMER1_DATA = TIMER_FREQ_1024(10);
    TIMER1_CR = TIMER_ENABLE | TIMER_DIV_1024 | TIMER_IRQ_REQ ;

    //Set interrupt handlers
    irqSet(IRQ_TIMER0, &hitTimerISR);
    irqSet(IRQ_TIMER1, &waitTimerISR);
}

int getWait() {
	return wait;
}

void waitTimerISR() {
    if(wait > 0) {
        --wait;
    }
    if(wait == 0) {
        stopWaitTimer();
    }
}

void setWait(int i) {
	wait = i;
}

void hitTimerISR() {
    if(p1Cooldown > 0) {
        --p1Cooldown;
    }
    if(p2Cooldown > 0) {
        --(p2Cooldown);
    }

    if((p1Cooldown == p2Cooldown) && (p2Cooldown == 0)) {
        stopHitTimer();
    }
}

void startHitTimer() {
    //Enable timer0 interrupts
    irqEnable(IRQ_TIMER0);
}

void stopWaitTimer() {
    //Disable timer0 interrupts
    irqDisable(IRQ_TIMER1);
}

void startWaitTimer() {
    //Enable timer0 interrupts
    irqEnable(IRQ_TIMER1);
}

void stopHitTimer() {
    //Disable timer0 interrupts
    irqDisable(IRQ_TIMER0);
}
