#pragma once

#include <nds.h>
#include "utils.h"


/**
 * @brief Interrupt service routine for timer 0
 */ 
void hitTimerISR();

void waitTimerISR();

void setWait(int i);

int getWait();

/**
 * @brief initializes a timer to generate an interrupt every second
 */
void initTimer();

/**
 * @brief Enables all interrupts related to the timer
 */
void startHitTimer();

/**
 * @brief Disables all interrupts related to the timer
 */
void stopHitTimer();

/**
 * @brief  Disables all interrupts related to the wait timer
 */
void stopWaitTimer();

/**
 * @brief Enables all interrupts related to the wait timer
 */
void startWaitTimer();
