#pragma once

#include <nds.h>
#include <maxmod9.h>
#include "soundbank.h"
#include "soundbank_bin.h"

/**
 * @brief Initializes the NDS's sound engine
 */ 
void soundInit();

/**
 * @brief Plays a sound FX
 * @param i decides on which sound to play
 */
void soundPlaySoundFX(int i);

/**
 * @brief Plays the BG music in a loop
 */ 
void soundPlayMusic();
