/*
 * Capture the flag Nintendo DS
 */

#include <stdio.h>
#include <ctype.h>
#include "utils.h"
#include "game.h"

int main(void) {
    //Inititalize game
    game_t* game = initGame(P1_VS_P2);

    //Main gameplay loop
    while(1) {

        //read input
        char input = 0;
        input = tolower(fgetc(stdin));
        switch(input) {
            case 'w':
                if(!checkForCollisions(game, game->player1, 0, 8)) {
                    game->player1->y += 8;
                }
                break;
            case 'a':
                if(!checkForCollisions(game, game->player1, 0, 8)) {
                    game->player1->x -= 8;
                }
                break;
            case 's':
                if(!checkForCollisions(game, game->player1, 0, 8)) {
                    game->player1->y -= 8;
                }
                game->player1->y -= 8;
                break;
            case 'd':
                if(!checkForCollisions(game, game->player1, 0, 8)) {
                    game->player1->x += 8;
                }
                break;
            default:
                break;
        }
        //Check that the user doesn't want to exit the game
        if(input == 'q') {
            break;
        }

        //Update the game's logical state
        updateGameState(game);
        printState(game);
    }

    //End the game and return 
    endGame(game);
    return ERR_NONE;
}
