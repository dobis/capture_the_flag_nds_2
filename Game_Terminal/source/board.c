#include "board.h"

void initBoard(board_t board) {
    //Sanity check
    REQUIRE_NON_NULL(board);

    //Clear the board
    clearBoard(board);

    //Place flags
    board[GRID_WIDTH + SIDE_WIDTH + BASE_WIDTH/2] = BASE_FLAG;
    board[(GRID_HEIGHT - 2) * GRID_WIDTH + SIDE_WIDTH + BASE_WIDTH/2] = BASE_FLAG;

    //Place players
    board[(GRID_HEIGHT - BASE_HEIGHT) * GRID_WIDTH + SIDE_WIDTH + BASE_WIDTH/2] = PLAYER;
    board[(BASE_HEIGHT) * GRID_WIDTH + SIDE_WIDTH + BASE_WIDTH/2] = PLAYER;

    return;
}

void updateSlot(board_t board, size_t x, size_t y, slot_t value) {
    //Sanity check
    REQUIRE_NON_NULL(board);

    size_t private_x, private_y;

    //Check that the givent coordinates aren't out of bounds
    private_x = x > GRID_WIDTH - 1 ? GRID_WIDTH - 1 : x;
    private_y = y > GRID_HEIGHT - 1 ? GRID_HEIGHT - 1 : y;

    board[(private_y * GRID_WIDTH) + private_x] = value;

    return;
}

slot_t getSlot(board_t board, size_t x, size_t y) {
    //Sanity Check
    REQUIRE_NON_NULL_VAL(board, NONE);

    uint8_t log_x = x / BOARD_GRANULARITY;
    uint8_t log_y = y / BOARD_GRANULARITY;

    //Check that the coordinates are in bounds
    REQUIRE(log_x < GRID_WIDTH);
    REQUIRE(log_y < GRID_HEIGHT);

    return board[(log_y * GRID_WIDTH) + log_x];
}

uint8_t isInBase(board_t board, size_t x, size_t y) {
    //Sanity check 
    REQUIRE_NON_NULL_VAL(board, ERR);

    return (x >= SIDE_WIDTH && x < SIDE_WIDTH + BASE_WIDTH) && (y < BASE_HEIGHT || y >= GRID_HEIGHT - BASE_HEIGHT);
}

void clearBoard(board_t board) {
    //Sanity check 
    REQUIRE_NON_NULL(board);

    //Loop indexes
    size_t x, y;

    //Fill the grid with initial state (cf::board.h)
    for (y = 0; y < GRID_HEIGHT; ++y) {
        for(x = 0; x < GRID_WIDTH; ++x) {
            if(isInBase(board, x, y)) {
                board[(y * GRID_WIDTH) + x] = BASE;
            } else {
                board[(y * GRID_WIDTH) + x] = EMPTY;
            }
        }
    }

    return;
}
