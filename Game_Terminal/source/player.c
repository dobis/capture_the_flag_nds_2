#include "player.h"

void initPlayer(player_t* player, struct flag* flag, enum player_type pt) {
    //Sanity check
    REQUIRE_NON_NULL(player);
    REQUIRE_NON_NULL(flag);
    REQUIRE(pt == PLAYER_1 || pt == PLAYER_2);

    //Initialize player
    player->flag = flag;
    player->has_flag = 0;

    switch (pt) {
    case PLAYER_1:
        player->x = PLAYER_1_INIT_X_POS;
        player->y = PLAYER_1_INIT_Y_POS;
        break;

    case PLAYER_2:
        player->x = PLAYER_2_INIT_X_POS;
        player->y = PLAYER_2_INIT_Y_POS;
        break;
    
    default:
        break;
    }
}

uint8_t playerGetXPos(player_t* player) {
    return player->x / BOARD_GRANULARITY;
}

uint8_t playerGetYPos(player_t* player) {
    return player->y / BOARD_GRANULARITY;
}

uint8_t checkForFlag(player_t* player, struct flag* flag) {
    //Sanity check 
    REQUIRE_NON_NULL_VAL(player, ERR);
    REQUIRE_NON_NULL_VAL(flag, ERR);

    return (playerGetXPos(player) == flagGetXPos(flag)) &&
           (playerGetYPos(player) == flagGetYPos(flag));
}   
