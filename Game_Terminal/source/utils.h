
#include <stdlib.h>
#include <stdio.h>



#define ERR -1
#define ERR_NONE 0

#define REQUIRE_NON_NULL(p) \
    do { \
        if(p == NULL) { \
            fprintf(stderr, "ERROR: Argument musn't be NULL!"); \
            return; \
        } \
    } while(0)

#define REQUIRE_NON_NULL_VAL(p, val) \
    do { \
        if(p == NULL) { \
            fprintf(stderr, "ERROR: Argument musn't be NULL!"); \
            return val; \
        } \
    } while(0)

#define REQUIRE(pred) \
    do { \
        if(!(pred)) { \
            fprintf(stderr, "ERROR: Invalid Argument !"); \
        } \
    } while(0)

#define FREE_AND_CLEAR(p) \
    do { \
        free(p); \
        p = NULL; \
    } while(0)


/**
 * EXPERIMENTAL, HASEN'T BEEN TESTED
 * @brief Prints out a message in desmumes debug console
 * @param errmsg the error message that should be printed out
 */
void desmumePrint(const char* errmsg);
