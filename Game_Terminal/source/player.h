#ifndef __PLAYER_H
#define __PLAYER_H

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "utils.h"
#include "flag.h"

#define MAX_SPRITE_NAME 256
#define PLAYER_1_INIT_X_POS 15 * 8
#define PLAYER_1_INIT_Y_POS 4 * 8
#define PLAYER_2_INIT_X_POS 15 * 8
#define PLAYER_2_INIT_Y_POS 43 * 8

enum player_type {
	PLAYER_1,
	PLAYER_2
};

typedef struct player {
    size_t x;
    size_t y;
    struct flag* flag;
    uint8_t has_flag;
} player_t;

/**
 * @brief Initializes a given player using the given flag 
 * @param player, a pointer to the player that will be initialized
 * @param flag, a pointer to the flag associated to the player
 */ 
void initPlayer(player_t* player, struct flag* flag, enum player_type pt);

/**
 * @brief Gets the given player's logical x coordinate on the board
 * @param player, a pointer to the player in question
 * @return the logical x coordinate of the player on the board
 */
uint8_t playerGetXPos(player_t* player);

/**
 * @brief Gets the given player's logical y coordinate on the board
 * @param player, a pointer to the player in question
 * @return the logical y coordinate of the player on the board
 */
uint8_t playerGetYPos(player_t* player);

/**
 * @brief Checks to see if the player has acquired the adversary's flag 
 * @param player, the player we want to check for 
 * @param flag, the adversary's flag that is going to be captured
 * @return 1 if the player has captured the flag, 0 otherwise and -1 in case of an error
 */
uint8_t checkForFlag(player_t* player, struct flag* flag);


#endif

