#ifndef __AICONTROLLER_H
#define __AICONTROLLER_H

#include <stdlib.h>
#include "utils.h"
#include "player.h"
#include "board.h"


/**
 * @brief returns the mahattan distance (discrete blockwise version of the euclidian distance)
 * between two given points.
 * @param x1 the logical x-coordinate of the first point
 * @param y1 the logical y-coordinate of the first point
 * @param x2 the logical x-coordinate of the second point
 * @param y2 the logical y-coordinate of the second point
 * @return the manhattan distance between the two points
 */
size_t manhattanDist(int x1, int y1, int x2, int y2);


/**
 * Computes the decision-score for a given slot depending on the game's state
 * @param board, the current state of the game defined by the board
 * @param x the logical x-coordinate of the slot that we want to attribute a score to
 * @param y the logical y-coordinate of the slot that we want to attribute a score to
 * @param depth the depth of the state prediction (WARNING: complexity = O(sizeof(board)^depth))
 * @return the minimax decision score associated to the slot
 */
size_t minimax(board_t board, int x, int y, size_t depth);

/**
 * @brief retruns the next move that the ai player should do in the form of an array containing 2 coordinates
 * @param board the current state of the game
 * @param ai the ai controlled player 
 * @return an array containing two ints: the logical x and y coordinates of the next move
 */
int* nextMove(board_t board, player_t* ai);

#endif
